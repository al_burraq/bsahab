<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();
});

Route::post('login','apiController@login');
Route::post('logout','apiController@logout');
Route::post('getUserMessages','apiController@getUserMessages');
Route::post('getPickAndDrop','apiController@getPickAndDrop');
Route::post('vehicleBooking','apiController@vehicleBooking');


//
