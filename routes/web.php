<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

  Route::prefix('admin')->group(function () {

    Route::get('testing','adminController@test');
    Route::get('/seo','adminController@seoIndex')->name('seoIndex');
    Route::get('/seoCreate','adminController@seoCreate')->name('seoCreate');
    Route::post('/seoStore','adminController@seoStore')->name('seoStore');
    Route::get('/seoEdit/{id}','adminController@seoEdit')->name('seoEdit');
    Route::post('/seoPut','adminController@seoPut')->name('seoPut');
    Route::get('/seoCollection','adminController@seoCollection')->name('seoCollection');
    Route::get('/','adminController@home')->name('adminHome');
    Route::post('/checkStatus','adminController@checkStatus')->name('checkStatus');
    Route::get('/subscriberData','adminController@subscriberData')->name('subscriberData');
    Route::get('/subscribLetterList','adminController@subscribLetterList')->name('subscribLetterList');
    Route::get('/userMessages','adminController@userMessages')->name('userMessages');
    Route::get('/getUserMessageJson','adminController@getUserMessageJson')->name('getUserMessageJson');
    Route::post('/setUserMessageSeen','adminController@setUserMessageSeen')->name('setUserMessageSeen');
    Route::post('/deleteUserMessage','adminController@deleteUserMessage')->name('deleteUserMessage');
    Route::post('/deleteDestination','adminController@deleteDestination')->name('deleteDestination');
    Route::get('/destination','adminController@destination')->name('adminDestination');
    Route::get('/createDestination','adminController@createDestination')->name('createDestination');
    Route::post('/addDestination','adminController@addDestination')->name('addDestination');
    Route::get('/editDestination/{id}','adminController@editDestination')->name('editDestination');
    Route::post('/updateDestination/{id}','adminController@updateDestination')->name('updateDestination');
    Route::get('/vehicles','adminController@vehicles')->name('adminVehicles');
    Route::get('/createVehicle','adminController@createVehicle')->name('createVehicle');
    Route::post('/addVehicle','adminController@addVehicle')->name('addVehicle');
    Route::get('/editVehicle/{id}','adminController@editVehicle')->name('editVehicle');
    Route::post('/updateVehicle/{id}','adminController@updateVehicle')->name('updateVehicle');
    Route::post('/deleteVehicle','adminController@deleteVehicle')->name('deleteVehicle');
    Route::post('/deleteSubsriber','adminController@deleteSubsriber')->name('deleteSubsriber');
    Route::post('/deleteGalleryImage','adminController@deleteGalleryImage')->name('deleteGalleryImage');
    Route::get('/vehicleBooking','adminController@vehicleBooking')->name('vehicleBooking');
    Route::post('/getVehicleBooking','adminController@getVehicleBooking')->name('getVehicleBooking');
    Route::post('/deleteBooking','adminController@deleteBooking')->name('deleteBooking');
    Route::post('setBookingSeen','adminController@setBookingSeen')->name('setBookingSeen');
    Route::get('/pickDrop','adminController@pickDrop')->name('pickDropAdmin');
    Route::post('/getPickDrop','adminController@getPickDrop')->name('getPickDrop');
    Route::post('/deletePickAndDrop','adminController@deletePickAndDrop')->name('deletePickAndDrop');
    Route::post('/setPickAndDrop','adminController@setPickAndDrop')->name('setPickAndDrop');
    Route::post('/getInitilizationData','adminController@getInitilizationData')->name('getInitilizationData');
    Route::post('/deleteDestinationGalleryImage','adminController@deleteDestinationGalleryImage')->name('deleteDestinationGalleryImage');
});

Auth::routes();

Route::post('getVehcilesJson','VehicleController@getVehcilesJson')->name('getVehcilesJson');
Route::resource('pickAndDrop','PickAndDropController');
Route::get('/', 'HomeController@index')->name('ahome');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home',function(){
  return Redirect::to('/');
});
Route::post('/subscribNewLetter','NewsLetterController@store')->name('subscribNewLetter');
Route::post('/userMessageSave','UserMessageController@store')->name('userMessageSave');
Route::get('vehicle/{id}/{name?}','VehicleController@vehicle');
Route::resource('/rental-vehicles','VehicleController');
Route::resource('/services','ServiceController');
Route::get('/rent-a-car','ServiceController@rentACar')->name('rent-a-Car');
Route::get('/tour-management','ServiceController@tourManagement')->name('tour-management');
Route::get('/pick-and-drop','ServiceController@pickAndDrop')->name('pick-and-drop');
Route::get('/policy',function(){
  return view('policy');
})->name('policy');
Route::get('/tourist-attraction','DestinationController@provinces')->name('provinces');
Route::post('/car-Booking',"carBookingController@store")->name('car-Booking');
Route::get('/tourist-attraction-Detail/{id}/{title?}','DestinationController@destination')->name('destination');
Route::get('/tourist-attraction/{id}/{title?}','DestinationController@provinceNew')->name('provinceNew');
Route::resource('/destinations','DestinationController');
Route::post('planTour','TourController@planeTour')->name('planeTour');
Route::resource('/plan-my-tour','TourController');
Route::get('contact-us','UserMessageController@index')->name('contact-us');
Route::get('/loyalty-card',function(){
  return view('loyalty-card');
})->name('loyalty-card');
