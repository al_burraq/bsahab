<?php
namespace App\Helpers;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use App\User;
use FCM;

class Helper
{
    public static function mail($subject, $message, $replayTo = null)
    {
        $to = "thegr8awais@gmail.com";
        $subject = "new car booking";

        if ($replayTo != null) {
            $headers = "Reply-To: ". strip_tags($replayTo) . "\r\n";
        } else {
            $headers = '';
        }
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        return mail($to, $subject, $message, $headers);
    }


    public static function response($message, $status, $dataA = [])
    {
        $data = (OBJECT)[
        'message'=>$message,
        'status'   =>$status
        ];
        foreach ($dataA as $key => $value) {
            $data->$key = $value;
        }
        return json_encode($data);
    }
    public static function clean($string)
    {
        $string = trim($string);
        return preg_replace('/[^A-Za-z0-9\-]\s{1,}/', '', $string); // Removes special chars.
    }
    public static function makeRichTextStoreable($text){
      return str_replace('"', "'", $text);
    }
    public static function makeRichTextDisplayAble($text){
      return str_replace("'", '"', $text);
    }
    public static function radioYesOrNo($checkbox)
    {
        if ($checkbox == "on") {
            return 'yes';
        } else {
            return 'no';
        }
    }
    public static function getGroupByMonth($collection)
    {
        $data = [];
        $data['01'] = 0;
        $data['02'] = 0;
        $data['03'] = 0;
        $data['04'] = 0;
        $data['05'] = 0;
        $data['06'] = 0;
        $data['07'] = 0;
        $data['08'] = 0;
        $data['09'] = 0;
        $data['10'] = 0;
        $data['11'] = 0;
        $data['12'] = 0;
        foreach ($collection as $obj) {
            $month = date('m', strtotime($obj->createdAt));
            $data["$month"] = $data["$month"] + 1;
        }
        return $data;
    }
    public static function make_thumb($src, $desired_width = 350)
    {
        $valueA = strripos($src, "/");
        $path = substr($src, 0, $valueA);
        $dir = $path."/thumbnail";
        $fileName = substr($src, $valueA+ 1);
        if (!file_exists($dir) && !is_dir($dir)) {
            mkdir($dir);
        }
        $destination = "$dir/$fileName";
        $dest = $destination;

        /* read the source image */
        $image_Type = '';
        $daata =  explode('.', $src);
        // echo $daata[1] . "<br>";
        if ($daata[1] == "jfif") {
            return 0;
        }
        // return 0;
        if ($daata[1] == 'jpeg' || $daata[1] == 'jpg') {
            $source_imageJpg = imagecreatefromjpeg($src);
        }

        if ($daata[1] == 'gif') {
            $source_imageGif = imagecreatefromgif($src);
        }

        if ($daata[1] == 'png') {
            $source_imagePng = imagecreatefrompng($src);
        }

        if (isset($source_imageJpg)) {
            $source_image = $source_imageJpg;
            $image_Type ='jpg';
        } elseif (isset($source_imagePng)) {
            $source_image = $source_imagePng;
            $image_Type =  'png';
        } elseif (isset($source_imageGif)) {
            $source_image = $source_imageGif;
            $image_Type = 'gif';
        }
        $width = imagesx($source_image);
        $height = imagesy($source_image);

        /* find the "desired height" of this thumbnail, relative to the desired width  */
        $desired_height = floor($height * ($desired_width / $width));
        /* create a new, "virtual" image */
        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
        /* copy source image at a resized size */
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
        /* create the physical thumbnail image to its destination */
        if ($image_Type =='jpg') {
            imagejpeg($virtual_image, $dest, 80);
        } elseif ($image_Type == 'png') {
            imagepng($virtual_image, $dest, 1);
        } elseif ($image_Type == 'gif') {
            imagegif($virtual_image, $dest);
        }
    }
    public static function loginCheck($token)
    {
        if (User::where('loginToken', '=', $token)->exists()) {
            return true;
        } else {
            return false;
        }
    }
    public static function sendNotification()
    {
      // FCM
      $users = User::where('deviceToken','!=',null)->where('deviceToken','!=','')->select('deviceToken')->get();
      $tokens = $users->toArray();
      if (count($tokens)) {
        self::FCM($tokens);
      }
    }
    public static function FCM($tokens,$title = null,$body = null)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('New Request');
        $notificationBuilder->setBody('Please Check your admin panel')
                    ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'my_data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

// You must change it to get your tokens
        $tokens = MYDATABASE::pluck('fcm_token')->toArray();

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();

//return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();

//return Array (key : oldToken, value : new token - you must change the token in your database )
        $downstreamResponse->tokensToModify();

//return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();

// return Array (key:token, value:errror) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
    }
}








//
