<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{
  protected $table = "image_gallery";
  public $timestamps = false;
}
