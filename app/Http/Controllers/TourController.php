<?php

namespace App\Http\Controllers;

use App\tour;
use App\GlobalModel as general;
use App\vehicle;
use App\Helpers\Helper;

use App\destination;
use Illuminate\Http\Request;


class TourController extends Controller
{
    public function planeTour(Request $request){
      $vehicleId = $request->vehicle;
      $destinationId = $request->destination;
      $buget       = $request->buget;
      $distance    = $request->distance;
      $destination = destination::where('destination_id','=',$destinationId)->first();
      $vehicle     = vehicle::where('id', '=',$vehicleId)->first();
      $petrolePrice = general::where('fieldName','=','petrolePrice')->first();
      if (!count($petrolePrice)) {
        return Helper::response('Petrole Price not found',0);
      }

      $distanceKmOnly = explode(' ', $distance);
      $distanceDouble = 2 * (int)$distanceKmOnly[0];

      $x               = $distanceDouble / $vehicle->mile_age;

      $pertrolExpances = $x * $petrolePrice->value;
      $remainingAmount = $buget - $pertrolExpances;
      $days               = $remainingAmount / $vehicle->per_day_rate;
      $days             = round($days);
      // dd($pertrolExpances,$remainingAmount,$remainingAmount);

      // travel buget
      $travelBuget = $pertrolExpances + ($vehicle->per_day_rate * $days);
      // dd($travelBuget,$vehicle->per_day_rate,$days,$pertrolExpances);
      if ($days < 2) {
        return Helper::response('Please increase Your buget and try again',0);
      }
      $data = (object)[
        'vehcile'=>$vehicle,
        'days'=>(int)$days,
        'totalDistance'=>$distance,
        'destination'=>$destination,
        'travelBuget'=>$travelBuget
      ];
      return Helper::response('success',1,$data);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = vehicle::all();
        $destinations = destination::all();
        return view('tour.index',compact('vehicles','destinations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function show(tour $tour)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function edit(tour $tour)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tour $tour)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function destroy(tour $tour)
    {
        //
    }
}
