<?php

namespace App\Http\Controllers;

use App\vehicle;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    public function getVehcilesJson(){
      // return vehicle::select('per_day_rate','id','name')->limit(5)->get();
      return vehicle::all();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalVehicleCount = vehicle::count();
        $vehcile = vehicle::all();
        $maxPrice = $vehcile->max('per_day_rate');
        $minPrice = $vehcile->min('per_day_rate');

        return view('vehicle.index',compact('totalVehicleCount','minPrice','maxPrice'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(vehicle $vehicle)
    {
        $id = $vehicle->id;

        $image_Gallery = \DB::table('image_gallery')->where('vehicleId','=',$id)->get();
        return view('vehicle.show',compact('vehicle','image_Gallery'));
    }
    public function vehicle($id,$title){
      // $id = $vehicle->id;
      $vehicle = vehicle::where('id','=',$id)->first();
      $image_Gallery = \DB::table('image_gallery')->where('vehicleId','=',$id)->get();
      return view('vehicle.show',compact('vehicle','image_Gallery'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(vehicle $vehicle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vehicle $vehicle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(vehicle $vehicle)
    {
        //
    }
}
