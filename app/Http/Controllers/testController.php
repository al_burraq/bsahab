<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use DB;
class testController extends Controller
{
    public function getUsers(Request $request)
    {
      return DB::table('angularJS')->join('angularJSphones','angularJS.id','angularJSphones.userId')->get();
    }
    public function saveUser(Request $request){
      if ($request->name == null) {
        return Helper::response('Name is missing',0);
      }
      if ($request->email == null) {
        return Helper::response('Email is missing',0);
      }
      if ($request->address == null) {
        return Helper::response('Address is missing',0);
      }
      if ($request->phone == null) {
        return Helper::response('Phone is missing',0);
      }
      $id = DB::table('angularJS')->insertGetId([
        "name"=> $request->name,
        "email"=> $request->email,
        "address"=> $request->address
      ]);
      DB::table('angularJSphones')->insert([
        "phone"=>$request->phone,
        "userId"=>$id
      ]);
      $user = DB::table('angularJS')->join('angularJSphones','angularJS.id','angularJSphones.userId')->where('angularJS.id','=',$id)->first();

      return Helper::response('User Saved',1,['user'=>$user]);
    }
    public function login(Request $request){
      if ($request->email == null) {
        return Helper::response('Email is missing',0);
      }
      if ($request->password == null) {
        return Helper::response('Password is missing',0);
      }
      if (!DB::table('angularJSUsers')->where('email','=',$request->email)->exists()) {
        return Helper::response("User with email - $request->email does not exists",0);
      }

      if (!DB::table('angularJSUsers')->where('email','=',$request->email)->where('password','=',$request->password)->exists()) {
        return Helper::response("User with given credentials does not exists",0);
      }else{
        $token = str_random(32);
        DB::table('angularJSUsers')->where('email','=',$request->email)->where('password','=',$request->password)->update([
          'token'=>$token
        ]);
        return Helper::response('Loggin successfully',1,[
          'token'=>$token
        ]);
      }

    }
    public function editUser(Request $request){
      if ($request->id == null) {
        return Helper::response('id is missing',0);
      }
      if ($request->name == null) {
        return Helper::response('Name is missing',0);
      }
      if ($request->email == null) {
        return Helper::response('Email is missing',0);
      }
      if ($request->address == null) {
        return Helper::response('Address is missing',0);
      }
      if ($request->phone == null) {
        return Helper::response('Phone is missing',0);
      }
      DB::table('angularJS')->where('id','=',$request->id)->update([
        "name"=> $request->name,
        "email"=> $request->email,
        "address"=> $request->address
      ]);
      DB::table('angularJSphones')->where('userId','=',$request->id)->update([
        "phone"=>$request->phone
      ]);
      return Helper::response('User updated',1);
    }

}
