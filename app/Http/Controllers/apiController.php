<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\User;
use App\userMessage;
use App\destination;
use App\vehicle;
use App\gallery;
use App\carBooking as vehicleBooking;
use App\pickAndDrop;
use DB;
use Auth;

class apiController extends Controller
{
    public function login(Request $request)
    {
        $email    = $request->email;
        $password = $request->password;
        $deviceToken = $request->deviceToken;
        if ($email == null) {
            return Helper::response('Email is missing.', 0);
        }
        if ($password == null) {
            return Helper::response('Password is missing.', 0);
        }
        if (!User::where('email', '=', $email)->exists()) {
            return Helper::response('Email does not exists in our Record.', 0);
        }
        if (!Auth::attempt(['email' => $email, 'password' => $password])) {
            return Helper::response('These credentials do not match our records.', 0);
        } else {
            $token = str_random(20).sha1(time());
            $id    = Auth::user()->id;

            if ($deviceToken != null) {
                User::where('id', '=', $id)->update([
                'loginToken'=>$token,
                'deviceToken'=>$deviceToken
                ]);
            } else {
                User::where('id', '=', $id)->update([
                'loginToken'=>$token
                ]);
            }
            return Helper::response('Login Successfully', 1, [
            'token'=>$token
            ]);
        }
    }
    public function logout(Request $request)
    {
        $loginToken = $request->token;
        $email      = $request->email;

        if (!User::where('loginToken', '=', $loginToken)->where('email', '=', $email)->exists()) {
            return Helper::response('You are not logged in', 0);
        }
        User::where('loginToken', '=', $loginToken)->where('email', '=', $email)->update([
        'loginToken'=>''
        ]);
        return Helper::response('You are logout', 1);
    }
    public function getUserMessages(Request $request)
    {
        $loginToken = $request->token;
        if ($loginToken == null) {
            return Helper::response('Token is missing', 0);
        }
        if (!Helper::loginCheck($loginToken)) {
            return Helper::response('You are not logged in.', 0);
        }
        return Helper::response('success', 1, [
        'messages'=>userMessage::all()
        ]);
    }
    public function getPickAndDrop(Request $Request)
    {
        $loginToken = $Request->token;
        if ($loginToken == null) {
            return Helper::response('Token is missing', 0);
        }
        if (!Helper::loginCheck($loginToken)) {
            return Helper::response('You are not logged in.', 0);
        }
        return Helper::response('success', 1, [
        'pickAndDrop'=>pickAndDrop::all()
        ]);
    }

    public function vehicleBooking(Request $Request)
    {
        $loginToken = $Request->token;
        if ($loginToken == null) {
            return Helper::response('Token is missing', 0);
        }
        if (!Helper::loginCheck($loginToken)) {
            return Helper::response('You are not logged in.', 0);
        }
        return Helper::response('success', 1, [
        'vehicleBooking'=>vehicleBooking::all()
        ]);
    }
    public function test(){
      Helper::sendNotification();
    }
    public function topDestinations() {
      // $topDestinations = 
    }
}
