<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\carBooking;
use Helper;


class carBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pickLocation = $request->pickLocation;
        $dropLocation = $request->dropLocation;
        $car          = $request->car;
        $phon         = $request->phon;
        $pickDate     = $request->pickDate;
        $dropDate     = $request->dropDate;
        if ($pickLocation == null || $dropLocation == null  || $car == null  || $phon == null || $pickDate == null  || $dropDate  == null ) {
          return 'emptyfields';
        }
        $carBooking = new carBooking;
        $carBooking->pickUpLocation = $pickLocation;
        $carBooking->dropLocation = $dropLocation;
        $carBooking->vehicle = $car;
        $carBooking->phone = $phon;
        $carBooking->pickDate	 = $pickDate;
        $carBooking->dropDate   = $dropDate;
        $carBooking->createdAt  = date('Y-m-d H:i:s');
        $carBooking->save();
        $message = <<<MESSAGE
        "Pick up Location: " $pickLocation <br>
        "Drop Location   : " $dropLocation <br>
        "Vehicle         : " $car <br>
        "Phone Number    : " $phon <br>
        "Pick up Date    : " $pickDate <br>
        "Drop Date       : " $dropDate <br>
MESSAGE;
        // dd($message);
        helper::mail('New car booking',$message);
        helper::sendNotification();
        return 'true';

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
