<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\userMessage;
use Helper;

class UserMessageController extends Controller
{
    public function index(){
      return view('contact-us');
    }
    public function store(Request $request){
      $name = $request->name;
      $email = $request->email;
      $message = $request->message;
      if ($name == null) {
        return "noName";
      }
      if ($email == null) {
        return "noEmail";
      }
      if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return "invalidEmail";
      }
      if ($message == null) {
        return "noMessage";
      }
      $userMessage = new userMessage;
      $userMessage->name = $name;
      $userMessage->email = $email;
      $userMessage->message = $message;
      $userMessage->createdAt  = date('Y-m-d H:i:s');
      $userMessage->save();

      $message = <<<MESSAGE
      "We have a new Message<br>
      "From       : " $email <br>
      "Name       : " $name <br>
      "Message    : " $message
MESSAGE;
      helper::mail('contact us from bsahab',$message,$email);
      Helper::sendNotification();
      return 'true';
    }
}
