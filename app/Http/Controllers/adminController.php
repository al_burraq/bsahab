<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use Redirect;
use App\Helpers\Helper;
use Session;
use Storage;
use File;
use App\userMessage;
use App\destination;
use App\vehicle;
use App\gallery;
use App\carBooking as vehicleBooking;
use App\pickAndDrop;
use App\destinationGallery as destinationGallery;

use DB;

class adminController extends Controller
{
  // public function
  public function __construct()
  {
      $this->middleware('auth');
  }
  public function seoCreate(){
    return view('admin.seoCreate');
  }
  public function seoIndex(){
    return view('admin.seoIndex');
  }
  public function seoEdit($id){
    $seo = DB::table('seo')->where('id','=',$id)->first();
    return view('admin.seoEdit',[
      'seo'=>$seo
    ]);
  }
  public function seoStore(Request $request){

    $url = $request->url;
    $id = $request->id;
    $title = $request->title;
    $description = $request->description;
    $tags = $request->tags;
    DB::table('seo')->insert([
      'url'=>$url,
      'title'=>$title,
      'desciption'=>$description,
      'keywords'=>$tags
    ]);
    Session::flash('status','Edit successfully');
    return Redirect::to(route('seoIndex'));

  }
  public function seoPut(Request $request){
    // dd($request->all());
    $url = $request->url;
    $id = $request->id;
    $title = $request->title;
    $description = $request->description;
    $tags = $request->tags;
    DB::table('seo')->where('id','=',$id)->update([
      'url'=>$url,
      'title'=>$title,
      'desciption'=>$description,
      'keywords'=>$tags
    ]);
    Session::flash('status','Edit successfully');
    return Redirect::to(route('seoIndex'));
  }
  public function seoCollection(){
    return DB::table('seo')->get();
  }
  public function subscribLetterList(){

    return view('admin.subscribLetterList');
    // return $subscriberList;
  }
  public function subscriberData(){
    return db::table('newsLetters')->get();
  }
  public function checkStatus(){
    $unseenVehicles = vehicleBooking::where('isReaded','=','unseen')->count();
    $unseenPickAndDrop = pickAndDrop::where('isReaded','=','unseen')->count();
    $unseenUserMessages = userMessage::where('isReaded','=','unseen')->count();
    return [
      'vehicleBooking'=>$unseenVehicles,
      'pickAndDrop'=>$unseenPickAndDrop,
      'userMessage'=>$unseenUserMessages
    ];
  }
  public function home(){
    return view('admin.home');
  }
  public function getUserMessageJson(){
    return userMessage::all();
  }
  public function deleteUserMessage(Request $request){
    userMessage::destroy($request->id);
    return 'true';
  }
  public function setUserMessageSeen(Request $request){
    $message = userMessage::find($request->id);
    $message->isReaded = $request->isReaded;
    $message->save();
    return 'true';
  }
  public function userMessages(){
    $messages = userMessage::all();
    return view('admin.userMessages',compact('messages'));
  }
  public function destination(){
    $destinations = destination::all();
    return view('admin.destination',compact('destinations'));
  }
  public function deleteDestination(Request $request){
    $destination = destination::find($request->id);
    $imagePath = $destination->image;
    $filename = public_path() . "/img/$imagePath";
    File::delete($filename);
    $filenameA = public_path() . "/img/thumbnail/$imagePath";
    File::delete($filenameA);

    $galleryImages = destinationGallery::where('destinationId','=',$request->id)->pluck('image');
    foreach ($galleryImages as $value) {
      $filename = public_path() . "/img/$request->id/$value";
      File::delete($filename);
    }

    destination::destroy($request->id);
  }
  public function createDestination(){
    return view('admin.createDestination');
  }
  public function addDestination(Request $request){
    $title      = $request->title;
    $longitude = $request->longitude;
    $latitude   = $request->latitude;
    $parent     = $request->parent;
    $emergencyno= $request->emergencyno;
    $image      = $request->image;
    $province = $request->province;
    $description = $request->description;
    $videoLink   = $request->videoLink;
    $galleryImages        = $request->galleryImages;
    if ($title == null || $longitude == null || $latitude == null || $parent == null
      || $emergencyno == null || $image == null || $province == null || $description == null
      ) {
      session::flash('status','Please provide all details');
      return Redirect::back();
    }
    $title       = Helper::clean($title);
    $description = Helper::makeRichTextStoreable($description);
    $destination = new destination;
    $destination->emergency_number = $emergencyno;
    $destination->title = $title;
    $destination->description = $description;
    $destination->province = $province;
    $destination->parent = $parent;
    $destination->longitude = $longitude;
    $destination->latitude = $latitude;
    $destination->videoLink = $videoLink;
    $path = Storage::disk('uploads')->putFile('img', $request->file('image'));
    $aPath = $path;

    $path = explode("/",$path);
    $abccc = Storage::disk('uploads')->getAdapter()->getPathPrefix();
    $totalPath = $abccc.$aPath;
    Helper::make_thumb($totalPath);
    $imagePath = $path[1];
    $destination->image = $imagePath;
    $destination->save();
    $destinationId = $destination->destination_id;
    if ($galleryImages != null) {
      foreach ($galleryImages as $key => $value) {
        $path = Storage::disk('uploads')->putFile('img/'.$destinationId, $galleryImages[$key]);
        $path = explode("/",$path);
        $imagePath = $path[2];
        destinationGallery::insert([
        'destinationId'=>$destinationId,
        'image'=>$imagePath
        ]);
      }
    }
    Session::flash('status','Destination saved successfully');
    return Redirect::to(route('adminDestination'));
  }
  public function updateDestination(Request $request,$id){
    $title      = $request->title;
    $longitude  = $request->longitude;
    $latitude   = $request->latitude;
    $parent     = $request->parent;
    $emergencyno= $request->emergencyno;
    $image      = $request->image;
    $province   = $request->province;
    $description= $request->description;
    $videoLink  = $request->videoLink;
    $galleryImages = $request->galleryImages;
    $title       = Helper::clean($title);
    $description = Helper::makeRichTextStoreable($description);

    $destination = destination::find($id);
    $destination->emergency_number = $emergencyno;
    $destination->title = $title;
    $destination->description = $description;
    $destination->province = $province;
    $destination->parent = $parent;
    $destination->longitude = $longitude;
    $destination->latitude = $latitude;
    $destination->videoLink = $videoLink;

    if ($request->hasFile('image')) {
      $path = Storage::disk('uploads')->putFile('img', $request->file('image'));
      $aPath = $path;
      $path = explode("/",$path);
      $imagePath = $path[1];
      $destination->image = $imagePath;
      $abccc = Storage::disk('uploads')->getAdapter()->getPathPrefix();
      $totalPath = $abccc.$aPath;
      Helper::make_thumb($totalPath);

    }
    $destination->save();
    if ($galleryImages != null) {
      foreach ($galleryImages as $key => $value) {
        $path = Storage::disk('uploads')->putFile('img/'.$destinationId, $galleryImages[$key]);
        $path = explode("/",$path);
        $imagePath = $path[2];
        destinationGallery::insert([
          'destinationId'=>$destinationId,
          'image'=>$imagePath
        ]);
      }
    }
    Session::flash('status','Destination updated successfully');
    return Redirect::to(route('adminDestination'));
  }

  public function editDestination($id){
    $destination = destination::find($id);
    $destinationGallary = destinationGallery::where('destinationId','=',$id)->get();
    return view('admin.editDestination',compact('destination','destinationGallary'));
  }
  public function deleteDestinationGalleryImage(Request $request){
    destinationGallery::where('id','=',$request->id)->delete();

  }
  public function vehicles(){
    $vehicles = vehicle::all();
    return view('admin.vehicles',compact('vehicles'));
  }
  public function createVehicle(){
    return view('admin.addVehicle');
  }
  public function editVehicle($id){
    $vehicle = vehicle::find($id);
    $gallery = gallery::where('vehicleId','=',$id)->get();
    return view('admin.editVehicle',compact('vehicle','gallery'));
  }
  public function deleteGalleryImage(Request $request){
    $id = $request->id;
    $galleryImage = gallery::find($id);
    $filename = public_path() . "/img/$galleryImage->vehicleId/$galleryImage->vehicleImage";
    $filenameThumnnail = public_path() . "/img/thumbnail/$galleryImage->vehicleId/$galleryImage->vehicleImage";
    File::delete($filenameThumnnail);
    File::delete($filename);
    $galleryImage->destroy($id);
    return 'true';
  }
  public function updateVehicle(Request $request,$id){
    $name                 = $request->name;
    $vehicleMaker         = $request->vehicleMaker;
    $model                = $request->model;
    $passenger            = $request->passenger;
    $baggageQuantity      = $request->baggageQuantity;
    $doors                = $request->doors;
    $perDayRate           = $request->perDayRate;
    $mileAge              = $request->mileAge;
    $phoneNumber          = $request->phoneNumber;
    $tilt_steering_wheel  = $request->tilt_steering_wheel;
    $lcd                  = $request->lcd;
    $power_door_locks     = $request->power_door_locks;
    $satellite_navigation = $request->satellite_navigation;
    $air_conditioning     = $request->air_conditioning;
    $climate_controll     = $request->climate_controll;
    $engine               = $request->engine;
    $manual_transmission  = $request->manual_transmission;
    $image                = $request->image;
    $galleryImages        = $request->galleryImages;

    if ( $phoneNumber == null
    || $mileAge == null || $perDayRate == null
    || $doors == null || $baggageQuantity == null
    || $passenger == null || $model == null
    || $vehicleMaker == null || $name == null) {
      session::flash('status','Please provide all details');
      return Redirect::back();
    }

    $vehicle = vehicle::find($id);
    $vehicle->name                  = $name;
    $vehicle->vehicle_make          = $vehicleMaker;
    $vehicle->model                 = $model;
    $vehicle->passenger             = $passenger;
    $vehicle->doors                 = $doors;
    $vehicle->baggage_quantity      = $baggageQuantity;
    $vehicle->per_day_rate          = $perDayRate;
    $vehicle->phone_number          = $phoneNumber;
    $vehicle->mile_age              = $mileAge;
    $vehicle->vehicle_make          = $vehicleMaker;
    $vehicle->lcd                   = Helper::radioYesOrNo($lcd);
    $vehicle->power_door_locks      = Helper::radioYesOrNo($power_door_locks);
    $vehicle->air_conditioning      = Helper::radioYesOrNo($air_conditioning);
    $vehicle->climate_controll      = Helper::radioYesOrNo($climate_controll);
    $vehicle->tilt_steering_wheel   = Helper::radioYesOrNo($tilt_steering_wheel);
    $vehicle->satellite_navigation  = Helper::radioYesOrNo($satellite_navigation);
    $vehicle->manual_transmission   = (Helper::radioYesOrNo($manual_transmission) == "yes" ? 'automatic' : 'manual');
    $vehicle->engine                = $engine;

    if ($request->hasFile('image')) {
      $filename = public_path() . "/img/$vehicle->id/$vehicle->image";
      $filenameA = public_path() . "/img/thumbnail/$vehicle->id/$vehicle->image";
      File::delete($filename);
      File::delete($filenameA);

      $path = Storage::disk('uploads')->putFile('img/'.$vehicle->id, $request->file('image'));
      $aPath = $path;
      $path = explode("/",$path);
      $imagePath = $path[2];
      $imgPath = $imagePath;
      $vehicle->image = $imgPath;
      $abccc = Storage::disk('uploads')->getAdapter()->getPathPrefix();
      $totalPath = $abccc.$aPath;
      Helper::make_thumb($totalPath);
    }
    if ($request->hasFile('galleryImages')) {
      foreach ($galleryImages as $key => $value) {
        $path = Storage::disk('uploads')->putFile('img/'.$vehicle->id, $galleryImages[$key]);
        $path = explode("/",$path);
        $imagePath = $path[2];
        gallery::insert([
          'vehicleId'=>$vehicle->id,
          'vehicleImage'=>$imagePath
        ]);
      }
    }
    $vehicle->save();
    session::flash('status','Vehicle update successfully');
    return Redirect::to(route('adminVehicles'));
  }
  public function addVehicle(Request $request){
    $name                 = $request->name;
    $vehicleMaker         = $request->vehicleMaker;
    $model                = $request->model;
    $passenger            = $request->passenger;
    $baggageQuantity      = $request->baggageQuantity;
    $doors                = $request->doors;
    $perDayRate           = $request->perDayRate;
    $mileAge              = $request->mileAge;
    $phoneNumber          = $request->phoneNumber;
    $tilt_steering_wheel  = $request->tilt_steering_wheel;
    $lcd                  = $request->lcd;
    $power_door_locks     = $request->power_door_locks;
    $satellite_navigation = $request->satellite_navigation;
    $air_conditioning     = $request->air_conditioning;
    $climate_controll     = $request->climate_controll;
    $engine               = $request->engine;
    $manual_transmission  = $request->manual_transmission;
    $image                = $request->image;
    $galleryImages        = $request->galleryImages;

    if ( $phoneNumber == null
    || $mileAge == null || $perDayRate == null
    || $doors == null || $baggageQuantity == null
    || $passenger == null || $model == null
    || $vehicleMaker == null || $name == null) {
      session::flash('status','Please provide all details');
      return Redirect::back();
    }
    if (!$request->hasFile('image')) {
      session::flash('status','Please provide all details');
      return Redirect::back();
    }
    if (!$request->hasFile('galleryImages')) {
      session::flash('status','Please provide all details');
      return Redirect::back();
    }

    $vehicle = new vehicle;
    $vehicle->name                  = $name;
    $vehicle->vehicle_make          = $vehicleMaker;
    $vehicle->model                 = $model;
    $vehicle->passenger             = $passenger;
    $vehicle->doors                 = $doors;
    $vehicle->baggage_quantity      = $baggageQuantity;
    $vehicle->per_day_rate          = $perDayRate;
    $vehicle->phone_number          = $phoneNumber;
    $vehicle->mile_age              = $mileAge;
    $vehicle->vehicle_make          = $vehicleMaker;
    $vehicle->lcd                   = Helper::radioYesOrNo($lcd);
    $vehicle->power_door_locks      = Helper::radioYesOrNo($power_door_locks);
    $vehicle->air_conditioning      = Helper::radioYesOrNo($air_conditioning);
    $vehicle->climate_controll      = Helper::radioYesOrNo($climate_controll);
    $vehicle->tilt_steering_wheel   = Helper::radioYesOrNo($tilt_steering_wheel);
    $vehicle->satellite_navigation  = Helper::radioYesOrNo($satellite_navigation);
    $vehicle->manual_transmission   = (Helper::radioYesOrNo($manual_transmission) == "yes" ? 'automatic' : 'manual');
    $vehicle->engine                = $engine;
    $vehicle->save();
    $path = Storage::disk('uploads')->putFile('img/'.$vehicle->id, $request->file('image'));
    $path = explode("/",$path);
    $imagePath = $path[2];
    $imgPath = $imagePath;
    $vehicle->image               = $imgPath;
    $vehicle->update();
    foreach ($galleryImages as $key => $value) {
      $path = Storage::disk('uploads')->putFile('img/'.$vehicle->id, $galleryImages[$key]);
      $path = explode("/",$path);
      $imagePath = $path[2];
      gallery::insert([
        'vehicleId'=>$vehicle->id,
        'vehicleImage'=>$imagePath
      ]);
    }

    session::flash('status','Vehicle saved successfully');
    return Redirect::to(route('adminVehicles'));
  }

  public function deleteVehicle(Request $request){
    $vehicle = vehicle::find($request->id);
    $imagePath = $vehicle->image;

    $filename = public_path() . "/img/$imagePath";
    $filenameA = public_path() . "/img/thumbnail/$imagePath";
    File::delete($filename);
    File::delete($filenameA);
    $galleryImages = gallery::where('vehicleId','=',$request->id)->pluck('vehicleImage');
    foreach ($galleryImages as $value) {
      $filename = public_path() . "/img/$value";
      File::delete($filename);
      $filenameA = public_path() . "/img/thumbnail/$value";
      File::delete($filenameA);
    }
    gallery::where('vehicleId','=',$request->id)->delete();
    vehicle::destroy($request->id);
    return 'true';
  }
  public function vehicleBooking(){
    return view('admin.vehicleBooking');
  }
  public function getVehicleBooking(){
    return vehicleBooking::orderBy('id','desc')->get();
  }
  public function deleteBooking(Request $request){
    $id = $request->id;
    vehicleBooking::where('id','=',$id)->delete();
    return 'true';
  }
  public function setBookingSeen(Request $request){
    $booking = vehicleBooking::find($request->id);
    $booking->isReaded = $request->isReaded;
    $booking->save();
    return 'true';
  }
  public function pickDrop(){
    return view('admin.pickDrop');
  }
  public function getPickDrop(){
    return pickAndDrop::orderBy('id','desc')->get();
  }
  public function deletePickAndDrop(Request $request){
    $id = $request->id;
    pickAndDrop::where('id','=',$id)->delete();
    return 'true';
  }
  public function setPickAndDrop(Request $request){
    $pickAndDrop = pickAndDrop::find($request->id);
    $pickAndDrop->isReaded = $request->isReaded;
    $pickAndDrop->save();
    return 'true';
  }
  public function getInitilizationData(){

    $date = strtotime(date('Y-m-d H:i:s').'-1 year');
    $date = date('Y-m-d', $date);
    $before1Year = $date;
    $messageRequestsData = userMessage::where('createdAt','>=',$before1Year)->get();
    $vehicleBookingData = vehicleBooking::where('createdAt','>=',$before1Year)->get();
    $pickAndDropData = pickAndDrop::where('createdAt','>=',$before1Year)->get();
    // dd($vehicleBookingData);
    $messageRequestsCount = count($messageRequestsData);
    $vehicleBookingCount       = count($vehicleBookingData);
    $pickAndDropCount       = count($pickAndDropData);
    // dd(Helper::getGroupByMonth($pickAndDropData),$pickAndDropData,$pickAndDropCount);
    return [
      'messageRequestsData'=>Helper::getGroupByMonth($messageRequestsData),
      'vehicleBookingData'=>Helper::getGroupByMonth($vehicleBookingData),
      'pickAndDropData'=>Helper::getGroupByMonth($pickAndDropData),
      'messageCount'=>$messageRequestsCount,
      'vehicleBookingCount'=>$vehicleBookingCount,
      'pickAndDrop'=>$pickAndDropCount
    ];
  }
  public function test(){
    dd($this->getInitilizationData());
    // $date = strtotime(date('Y-m-d H:i:s').'-1 year');
    // $date = date('Y-m-d', $date);
    // $before1Year = $date;
    // dd($pickAndDropData = pickAndDrop::where('createdAt','>=',$before1Year)->get());
  }
  public function deleteSubsriber(Request $reqeust,$id){
    DB::table('newsLetters')->where('id','=',$id)->delete();
  }
}















//
