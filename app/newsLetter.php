<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class newsLetter extends Model
{
    //
    public $timestamps = false;
    protected $table = "newsLetters";
}
