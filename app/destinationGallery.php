<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class destinationGallery extends Model
{
  protected $table = "destinationGallery";
  public $timestamps = false;
}
