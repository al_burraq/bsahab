<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class helperProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
      // App::bind('Helper', function()
      // {
      //     return new App\Helpers\Helper.php;
      // });
    }
}
