<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class destination extends Model
{
  protected $primaryKey = 'destination_id';
  public $timestamps = false;
}
