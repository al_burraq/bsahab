<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pickAndDrop extends Model
{
    protected $table = "pick-and-drop";
    public $timestamps = false;
}
