<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalModel extends Model
{
  protected $table = "Global";
  public $timestamps = false;
}
