@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')
  <link rel="stylesheet" href="{{asset("css/thumbnail-slider.css")}}">
  <link rel="stylesheet" href="{{asset("css/thumnail-slider.css")}}">
  <style media="screen">
    .booking-item-details .booking-item-header{
      border-top: none;
    }
  </style>
<div class="container">

  <div class="container">

              <div class="booking-item-details">
                  <header class="booking-item-header">
                      <div class="row">
                          <div class="col-md-9">
                              <h2 class="lh1em">{{$vehicle->vehicle_make}} {{$vehicle->name}} Available for Rent</h2>

                              <ul class="list list-inline text-small">
                                  <li><a href="mailto:bhaisahabtrans@gmail.com"><i class="fa fa-envelope"></i> E-mail Car Agent</a>
                                  </li>

                                  <li><i class="fa fa-phone"></i> +92-320-440-0993‬</li>
                              </ul>
                          </div>
                      </div>
                  </header>
                  <div class="row">
<div class="col-md-8">
  <div class="fotorama" data-width="700" data-ratio="700/467" data-max-width="100%" data-allowfullscreen="true"  data-loop="true"   data-nav="thumbs">
  @foreach ($image_Gallery as $element)
    <a href="{{asset("img/$element->vehicleId/$element->vehicleImage")}}">
    <img src="{{asset("img/$element->vehicleId/$element->vehicleImage")}}" width="494" height="370"></a>
  @endforeach
  </div>
</div>
<div class="col-md-4 pull-right">
      <p class="booking-item-header-price">
        <div style="font-size: 20px;">
          Price RS.<span style="font-size: 60px;">{{$vehicle->per_day_rate}}</span>/day
      </div>
      </p>
  <div class="booking-item-deails-date-location" style="    margin-top: 29px;">
    <ul>
        <li>
            <h5>Location:</h5>
            <p>Bhai Sahab Transport, Lahore, Pakistan</p>
        </li>
        <li>
            <h5>Open Now:</h5>
            <p><i class="fa fa-map-marker box-icon-inline box-icon-gray"></i>Bhai Sahab Transport  </p>
            <p><i class="fa fa-calendar box-icon-inline box-icon-gray"></i> 7 Days </p>
            <p><i class="fa fa-clock-o box-icon-inline box-icon-gray"></i> 24 Hours</p>
        </li>

    </ul>

  </div>
</div>
                  </div>

                  </div>
                  <div class="gap"></div>
                  <div class="row">
                    <div class="col-md-3">
                        <h5>Car Features</h5>
                        <ul class="booking-item-features booking-item-features-expand clearfix">

                                  <li rel="tooltip" data-placement="top" title="" data-original-title="Passengers Capacity"><i class="fa fa-male"></i><span class="booking-item-feature-title">Up to {{$vehicle->passenger}} Passengers</span>
                                  </li>
                                  <li rel="tooltip" data-placement="top" title="" data-original-title="Vehicle Model"><i class="fa fa-car"></i><span class="booking-item-feature-title"> {{$vehicle->name}} {{$vehicle->model}}</span>
                                  </li>
                                  <li rel="tooltip" data-placement="top" title="" data-original-title="No. of Doors"><i class="im im-car-doors"></i><span class="booking-item-feature-title">{{$vehicle->doors}} Doors</span>
                                  </li>
                                  <li rel="tooltip" data-placement="top" title="" data-original-title="Luggage Capacity"><i class="fa fa-briefcase"></i><span class="booking-item-feature-title">{{$vehicle->baggage_quantity}} bag of Laggage</span>
                                  </li>
                                  @if($vehicle->manual_transmission == 'manual')
                                  <li rel="tooltip" data-placement="top" title="" data-original-title="Transmission Type"><i class="im im-shift"></i><span class="booking-item-feature-title">{{$vehicle->manual_transmission}} Transmission</span>
                                  </li>
                                  @else
                                  <li rel="tooltip" data-placement="top" title="" data-original-title="Transmission Type"><i class="im im-shift-auto"></i><span class="booking-item-feature-title">{{$vehicle->manual_transmission}} Transmission</span>
                                  </li>
                                @endif

                        </ul>
                   </div>
                   <div class="col-md-3">
<h5>Default Equipment</h5>
<ul class="booking-item-features booking-item-features-expand clearfix">
@if ($vehicle->air_conditioning == "yes")
<li rel="tooltip" data-placement="top" title="" data-original-title="Air Conditioning"><i class="im im-air"></i><span class="booking-item-feature-title">Air Conditioning</span>
</li>
@endif
@if ($vehicle->lcd == 'yes')
<li rel="tooltip" data-placement="top" title="" data-original-title="LCD Display"><i class="fa fa-desktop"></i><span class="booking-item-feature-title">LCD Display</span>
</li>
@endif
@if ($vehicle->power_door_locks == "yes")
<li rel="tooltip" data-placement="top" title="" data-original-title="Power Doors"><i class="im im-car-window"></i><span class="booking-item-feature-title">Power Doors</span>
</li>
@endif
<li rel="tooltip" data-placement="top" title="" data-original-title="Price Per Day"><i class="fa fa-money"></i><span class="booking-item-feature-title">{{$vehicle->per_day_rate}} / Day </span>
</li>

@if ($vehicle->power_door_locks)
<li rel="tooltip" data-placement="top" title="" data-original-title="Power Locks"><i class="im im-lock"></i><span class="booking-item-feature-title">Power Locks</span>
</li>
@endif
                    </ul>
                    </div>


                    <div class="col-md-3">
                    <h5>Default Equipment</h5>
                    <ul class="booking-item-features booking-item-features-expand clearfix">
                    @if ($vehicle->satellite_navigation == "yes")
                      <li rel="tooltip" data-placement="top" title="" data-original-title="Navigation"><i class="im im-satellite"></i><span class="booking-item-feature-title">Navigation</span>
                      </li>
                    @endif
                     @if ($vehicle->tilt_steering_wheel == 'yes')
                      <li rel="tooltip" data-placement="top" title="" data-original-title="Steering Wheel"><i class="im im-car-wheel"></i><span class="booking-item-feature-title">Steering Wheel</span>
                      </li>
                    @endif
                    @if ($vehicle->climate_controll == 'yes')
                      <li rel="tooltip" data-placement="top" title="" data-original-title="Climate Control"><i class="im im-climate-control"></i><span class="booking-item-feature-title">Climate Control</span>
                      </li>
                    @endif
                    </ul>
                    </div>



                  </div>
                  <div class="gap gap-small"></div>
              </div>
              <div class="gap gap-small"></div>
          </div>
</div>
@endsection
