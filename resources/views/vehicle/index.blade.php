@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')
  <style media="screen">
    h5{
      color: white !important;
    }
    .rzslider .rz-bubble,.rzslider .rz-bubble.rz-limit {
      color: white !important;
    }
    /*#ed8323;*/
    .rzslider .rz-pointer,
    .rzslider .rz-selection {
      background-color: #ed8323 !important;
    }
    .rzslider .rz-pointer:after{
      background-color: #ed8323 !important;
    }
    .rzslider .rz-pointer.rz-active:after{
      background-color: transparent !important;
    }

    input[type=checkbox] {
        padding-left:5px;
        padding-right:5px;
        border-radius:15px;

        -webkit-appearance:button;

        border: double 1px white;

        background-color:#4d4d4d;
        color:#FFF;
        white-space: nowrap;
        overflow:hidden;
            border-radius: 1px;
        width: 22px;
        height: 22px;
    }

    input[type=checkbox]:checked {
      border: 1px solid #ed8323;
      background: #ed8323;
    }

    input[type=checkbox]:hover {
        box-shadow:0px 0px 10px #ed8323;
    }
    .cstmLabel {
      margin-top: -28px !important;
      margin-left: 27px !important;
    }
  </style>
  <script type="text/javascript">
    window.minPrice = {{$minPrice | 0}};
    window.maxPrice = {{$maxPrice}};
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
  <link rel="stylesheet" href="css/nouislider.css">
  <script src="js/nouislider.js" charset="utf-8"></script>
  <div ng-app="myApp" ng-controller="myController" class="container">

    <div class="mfp-with-anim mfp-hide mfp-dialog mfp-search-dialog" id="search-dialog">
        <h3>Search for Car</h3>
        <form>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                        <label>Pick-up From</label>
                        <input class="typeahead form-control" placeholder="City, Airport or U.S. Zip Code" type="text" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                        <label>Drop-off To</label>
                        <input class="typeahead form-control" placeholder="City, Airport or U.S. Zip Code" value="Same as Pick-up" type="text" />
                    </div>
                </div>
            </div>
            <div class="input-daterange" data-date-format="MM d, D">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                            <label>Pick-up Date</label>
                            <input class="form-control" name="start" type="text" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-clock-o input-icon input-icon-highlight"></i>
                            <label>Drop-off Time</label>
                            <input class="time-pick form-control" value="12:00 AM" type="text" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                            <label>Drop-off Date</label>
                            <input class="form-control" name="end" type="text" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-clock-o input-icon input-icon-highlight"></i>
                            <label>Pick-up Time</label>
                            <input class="time-pick form-control" value="12:00 AM" type="text" />
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary btn-lg" type="submit">Search for Flights</button>
        </form>
    </div>
    <h1 class="booking-title">Rental Cars</h1>
      <p>Bhai Sahab Transport offers stunning rental cars to meet your travel needs within low budget. Our all rental vehicles are fuel efficient and comfortable. If you are looking to hire car on rent, then you can get our massive range of rental cars that are mention below. Let’s choose your favourite cab within your own required budget. </p>
    <h2 class="booking-title">{{$totalVehicleCount}} Rental Vehicles Available in Lahore</h2>
    <div class="row">
        <div class="col-md-3" style="padding:0px;">
            <aside style="

            padding-right: 31px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            font-size: 11px;
            background: #4d4d4d;
            color: #fff;
            width: 100%;
            border: 1px solid #262626;

            ">
                <h3 style="    margin-top: 6px;
    color: white;
    font-size: 30px;
    margin-left: 38px;
    margin-bottom: 17px;">Filter By:</h3>
                <ul class="">
                    <li style="list-style:none;">
                        <h5 class="">Passengers Quantity</h5>
                        <div >
                          <input  ng-model="checkbox.five" id="five" type="checkbox" />
                            <label for="five" class="cstmLabel">
                                5 Passengers
                              </label>
                        </div>
                        <div >
                          <input ng-model="checkbox.six" type="checkbox" />
                            <label class="cstmLabel">
                              6 Passengers
                              </label>
                        </div>
                        <div >
                          <input ng-model="checkbox.seven" type="checkbox" />
                            <label class="cstmLabel">
                                7 Passengers</label>
                        </div>
                        <div >
                          <input  ng-model="checkbox.nine" type="checkbox" />
                            <label class="cstmLabel">
                                9 Passengers</label>
                        </div>
                        <div>
                          <input  ng-model="checkbox.twelve" type="checkbox" />
                            <label class="cstmLabel">
                                12 Passengers
                              </label>
                        </div>
                        <div >
                          <input  ng-model="checkbox.fourteeen" type="checkbox" />
                            <label class="cstmLabel">
                                14 Passengers
                            </label>
                        </div>
                        <div >
                          <input  ng-model="checkbox.twentyEight" type="checkbox" />
                            <label class="cstmLabel">
                                28 Passengers
                            </label>
                        </div>
                        <div >
                          <input  ng-model="checkbox.fourtyTwo" type="checkbox" />
                            <label class="cstmLabel">
                                42 Passengers
                            </label>
                        </div>
                    </li>
                    <hr style="
    border-top: 1px solid black;">
                    <li style="list-style:none;">
<h5 class="">Price</h5>
<input class="hidden" type="number" ng-model="minRangeSlider.minValue" />
{{-- <br/> --}}
<input class="hidden" type="number" ng-model="minRangeSlider.maxValue" />
{{-- <br/> --}}
<rzslider rz-slider-model="minRangeSlider.minValue" rz-slider-high="minRangeSlider.maxValue" rz-slider-options="minRangeSlider.options"></rzslider>



                    </li>





                </ul>
            </aside>
        </div>
        <div class="col-md-9">

            <ul class="booking-list">

@verbatim

<li ng-cloak ng-repeat="vehcile in vehciles |  pessengerFilter: checkbox | priceFilter: minRangeSlider | pagination: pagination ">
    <div class="booking-item" itemscope="" itemtype="https://auto.schema.org/Vehicle">
        <div class="row">
            <div class="col-md-3">
                <div class="booking-item-car-img">
                    <a href="vehicle/{{vehcile.id}}/{{cleanup(vehcile.vehicle_make)}}-{{cleanup(vehcile.name)}}-for-rent" itemprop="mainEntityOfPage"><img ng-cloak ng-if="vehcile.id" src="img/{{vehcile.id}}/{{vehcile.image}}" itemprop="image"></a>
                    <p class="booking-item-car-title" itemprop="name">{{vehcile.name}}</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">

<div class="col-md-12">
<ul class="booking-item-features booking-item-features-sign clearfix">
<li rel="tooltip" data-placement="top" title="Passengers" data-original-title="Passengers"><i  title="Passengers" class="fa fa-male"></i><span class="booking-item-feature-sign" itemprop="vehicleSeatingCapacity">x {{vehcile.passenger}}</span>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="Doors"><i title="Doors" class="im im-car-doors"></i><span class="booking-item-feature-sign" itemprop="numberOfDoors">x {{vehcile.doors}}</span>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="Baggage Quantity"><i title="Baggage Quantity" class="fa fa-briefcase"></i><span class="booking-item-feature-sign" itemprop="cargoVolume">x {{vehcile.baggage_quantity}}</span>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="Automatic Transmission"><i title="Automatic Transmission" class="im im-shift-auto"></i><span class="booking-item-feature-sign" itemprop="vehicleTransmission">{{vehcile.manual_transmission}}</span>
</li>
<li rel="tooltip" data-placement="top" title="" data-original-title="Petrol Vehicle"><i title="Petrol Vehicle" class="im im-diesel"></i><span class="booking-item-feature-sign" itemprop="vehicleTransmission">{{vehcile.engine}}</span>
</li>

</ul>
</div>

<div class="col-md-12">
<ul class="booking-item-features booking-item-features-small clearfix">
<li ng-show="vehcile.climate_controll === 'yes'" rel="tooltip" data-placement="top" title="" data-original-title="Climate Control"><i title="Climate Control" class="im im-climate-control"></i>
</li>
<li  ng-show="vehcile.air_conditioning === 'yes'" rel="tooltip" data-placement="top" title="" data-original-title="Air Conditioning"><i title="Air Conditioning" class="im im-air"></i>
</li>

<li ng-show="vehcile.power_door_locks === 'yes'" rel="tooltip" data-placement="top" title="" data-original-title="Power Door Locks"><i class="im im-lock" title="Power Door Locks"></i>
</li>

<li rel="tooltip" ng-show="vehcile.lcd === 'yes'" data-placement="top" title="" data-original-title="LCD">
<i class="fa fa-desktop" title="LCD" aria-hidden="true"></i>
</li>
</ul>
</div>

                </div>
            </div>
            <div class="col-md-3"><span class="booking-item-price">Rs.{{vehcile.per_day_rate}}</span><span>/day</span>
                <a href="vehicle/{{vehcile.id}}/{{cleanup(vehcile.vehicle_make)}}-{{cleanup(vehcile.name)}}-for-rent"><p class="booking-item-flight-class">
                        Premium</p><span class="btn btn-primary">Select</span></a>
            </div>
        </div>

    </div>
</li>



            </ul>
        </div>

        <div class="col-md-offset-6 col-md-4" ngCloak>
          <ul class="pagination" ngCloak>

            <li class="active"><a ng-click="previewsPage()" href="javascript:void(0)">Previous</a></li>
            <li  ng-cloak>&nbsp;&nbsp;&nbsp; Current Page: {{pagging.current}}&nbsp;&nbsp;</li>
            <li  ng-cloak>&nbsp;&nbsp;
              Total Pages: {{pagging.total}}
            &nbsp;&nbsp;</li>

            <li><a href="javascript:void(0)" ng-click="nextPage()">Next</a></li>

          </ul>
        </div>

    </div>
    <div class="gap"></div>
  @endverbatim

</div>
<link rel="stylesheet" href="css/rzslider.css">
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.14.3/ui-bootstrap-tpls.js"></script> --}}
<script src="https://rawgit.com/rzajac/angularjs-slider/master/dist/rzslider.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wnumb/1.1.0/wNumb.min.js" charset="utf-8"></script>

<script src="js/vehicleAng.js" charset="utf-8"></script>
@endsection
