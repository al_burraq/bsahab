<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>Bsahab</title>
  <link rel="apple-touch-icon" href="{{asset("assets/images/apple-touch-icon.png")}}">
  <link rel="shortcut icon" href="{{asset("assets/images/favicon.ico")}}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{asset("global/css/bootstrap.min.css")}}">
  <link rel="stylesheet" href="{{asset("global/css/bootstrap-extend.min.css")}}">
  <link rel="stylesheet" href="{{asset("assets/css/site.min.css")}}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{asset("global/vendor/animsition/animsition.css")}}">
  <link rel="stylesheet" href="{{asset("global/vendor/asscrollable/asScrollable.css")}}">
  <link rel="stylesheet" href="{{asset("global/vendor/switchery/switchery.css")}}">
  <link rel="stylesheet" href="{{asset("global/vendor/intro-js/introjs.css")}}">
  <link rel="stylesheet" href="{{asset("global/vendor/slidepanel/slidePanel.css")}}">
  <link rel="stylesheet" href="{{asset("global/vendor/flag-icon-css/flag-icon.css")}}">
  <link rel="stylesheet" href="{{asset("global/vendor/waves/waves.css")}}">
  <link rel="stylesheet" href="{{asset("global/vendor/chartist/chartist.css")}}">
  <link rel="stylesheet" href="{{asset("global/vendor/jvectormap/jquery-jvectormap.css")}}">
  <link rel="stylesheet" href="{{asset("global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css")}}">
  <link rel="stylesheet" href="{{asset("assets/examples/css/dashboard/v1.css")}}">
  <!-- Fonts -->
  <link rel="stylesheet" href="{{asset("global/fonts/material-design/material-design.min.css")}}">
  <link rel="stylesheet" href="{{asset("global/fonts/brand-icons/brand-icons.min.css")}}">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
  <!--[if lt IE 9]>
    <script src="{{asset("global/vendor/html5shiv/html5shiv.min.js")}}"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="{{asset("global/vendor/media-match/media.match.min.js")}}"></script>
    <script src="{{asset("global/vendor/respond/respond.min.js")}}"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="{{asset("global/vendor/breakpoints/breakpoints.js")}}"></script>
  <script src="{{asset("global/vendor/babel-external-helpers/babel-external-helpers.js")}}"></script>
  <script src="{{asset("global/vendor/jquery/jquery.js")}}"></script>

  <script>
  Breakpoints();
  </script>
  @yield('css')
</head>
<body class="animsition" >
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse"
  role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
      data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>
      <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse">
        <i class="icon md-more" aria-hidden="true"></i>
      </button>


    </div>
    <div class="navbar-container container-fluid">
      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
        <!-- Navbar Toolbar -->
        <ul class="nav navbar-toolbar">
          <li class="nav-item hidden-float" id="toggleMenubar">
            <a class="nav-link" data-toggle="menubar" href="#" role="button">
              <i class="icon hamburger hamburger-arrow-left">
                  <span class="sr-only">Toggle menubar</span>
                  <span class="hamburger-bar"></span>
                </i>
            </a>
          </li>
          <li class="nav-item hidden-sm-down" id="toggleFullscreen">
            <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
              <span class="sr-only">Toggle fullscreen</span>
            </a>
          </li>


        </ul>
        <!-- End Navbar Toolbar -->
        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">

          <li class="nav-item dropdown">
            <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
            data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="{{asset('img/default-profile.png')}}" alt="...">
                <i></i>
              </span>
            </a>
            <div class="dropdown-menu" role="menu">
              {{-- <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-card" aria-hidden="true"></i> Billing</a>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Settings</a>
              <div class="dropdown-divider"></div> --}}

              {{-- <a class="dropdown-item" onclick="accessPermission()" role="menuitem"><i class="icon fa fa-bell" aria-hidden="true"></i>Enable Notifications</a>
              <div class="dropdown-divider"></div> --}}
              <a class="dropdown-item"   onclick="event.preventDefault();document.getElementById('logout-form').submit();"  href="{{ route('logout') }}" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>

            </div>
          </li>



        </ul>
        <!-- End Navbar Toolbar Right -->

      </div>
      <!-- End Navbar Collapse -->
      <!-- Site Navbar Seach -->
      <div class="collapse navbar-search-overlap" id="site-navbar-search">
        <form role="search">
          <div class="form-group">
            <div class="input-search">
              <i class="input-search-icon md-search" aria-hidden="true"></i>
              <input type="text" class="form-control" name="site-search" placeholder="Search...">
              <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
              data-toggle="collapse" aria-label="Close"></button>
            </div>
          </div>
        </form>
      </div>
      <!-- End Site Navbar Seach -->
    </div>
  </nav>
  <div class="site-menubar">
    <div class="site-menubar-header">
      <div class="cover overlay">
        <img class="cover-image" src="{{asset('assets//examples/images/dashboard-header.jpg')}}"
        alt="...">
        <div class="overlay-panel vertical-align overlay-background">
          <div class="vertical-align-middle">
            <a class="avatar avatar-lg" href="javascript:void(0)">
              <img src="{{asset('img/default-profile.png')}}" alt="">
            </a>
            <div class="site-menubar-info">
              <h5 class="site-menubar-user">{{Auth::user()->name}}</h5>
              <p class="site-menubar-email">{{Auth::user()->email}}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu" data-plugin="menu">
            <li class="site-menu-item">
              <a class="animsition-link" href="{{route('adminHome')}}">
                <i class="site-menu-icon fa fa-home" aria-hidden="true"></i>
                <span class="site-menu-title">Home</span>
              </a>
            </li>
            <li class="site-menu-item ">
              <a class="animsition-link" href="{{route('userMessages')}}">
                <i class="site-menu-icon fa fa-envelope" aria-hidden="true"></i>
                <span class="site-menu-title">User Messages</span>
              </a>
            </li>
            <li class="site-menu-item ">
              <a class="animsition-link" href="{{route('adminDestination')}}">
                <i class="site-menu-icon fa fa-map-marker" aria-hidden="true"></i>
                <span class="site-menu-title">Destinations</span>
              </a>
            </li>
            <li class="site-menu-item ">
              <a class="animsition-link" href="{{route('adminVehicles')}}">
                <i class="site-menu-icon fa fa-car" aria-hidden="true"></i>
                <span class="site-menu-title">Vehicles</span>
              </a>
            </li>
            <li class="site-menu-item ">
              <a class="animsition-link" href="{{route('vehicleBooking')}}">
                <i class="site-menu-icon fa fa-shopping-cart" aria-hidden="true"></i>
                <span class="site-menu-title">Vehicle Booking</span>
              </a>
            </li>
            <li class="site-menu-item ">
              <a class="animsition-link" href="{{route('pickDropAdmin')}}">
                <i class="site-menu-icon fa fa-map-marker" aria-hidden="true"></i>
                <span class="site-menu-title">Pick And Drop</span>
              </a>
            </li>
            <li class="site-menu-item ">
              <a class="animsition-link" href="{{route('subscribLetterList')}}">
                <i class="site-menu-icon fa fa-envelope" aria-hidden="true"></i>
                <span class="site-menu-title">Subscriber</span>
              </a>
            </li>

            <li class="site-menu-item ">
              <a class="animsition-link" href="{{route('seoIndex')}}">
                <i class="site-menu-icon fa fa-free-code-camp" aria-hidden="true"></i>
                <span class="site-menu-title">Seo</span>
              </a>
            </li>



          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Page -->
  @yield('body')
  <!-- End Page -->
  <!-- Footer -->

  <!-- Core  -->
  <script src="{{asset("global/vendor/tether/tether.js")}}"></script>
  <script src="{{asset("global/vendor/bootstrap/bootstrap.js")}}"></script>
  <script src="{{asset("global/vendor/animsition/animsition.js")}}"></script>
  <script src="{{asset("global/vendor/mousewheel/jquery.mousewheel.js")}}"></script>
  <script src="{{asset("global/vendor/asscrollbar/jquery-asScrollbar.js")}}"></script>
  <script src="{{asset("global/vendor/asscrollable/jquery-asScrollable.js")}}"></script>
  <script src="{{asset("global/vendor/waves/waves.js")}}"></script>
  <!-- Plugins -->
  <script src="{{asset("global/vendor/switchery/switchery.min.js")}}"></script>
  <script src="{{asset("global/vendor/intro-js/intro.js")}}"></script>
  <script src="{{asset("global/vendor/screenfull/screenfull.js")}}"></script>
  <script src="{{asset("global/vendor/slidepanel/jquery-slidePanel.js")}}"></script>
  <script src="{{asset("global/vendor/jvectormap/jquery-jvectormap.min.js")}}"></script>
  <script src="{{asset("global/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js")}}"></script>
  <script src="{{asset("global/vendor/matchheight/jquery.matchHeight-min.js")}}"></script>
  <script src="{{asset("global/vendor/peity/jquery.peity.min.js")}}"></script>
  <!-- Scripts -->
  <script src="{{asset("global/js/State.js")}}"></script>
  <script src="{{asset("global/js/Component.js")}}"></script>
  <script src="{{asset("global/js/Plugin.js")}}"></script>
  <script src="{{asset("global/js/Base.js")}}"></script>
  <script src="{{asset("global/js/Config.js")}}"></script>
  <script src="{{asset("assets/js/Section/Menubar.js")}}"></script>
  <script src="{{asset("assets/js/Section/Sidebar.js")}}"></script>
  <script src="{{asset("assets/js/Section/PageAside.js")}}"></script>
  <script src="{{asset("assets/js/Plugin/menu.js")}}"></script>
  <!-- Config -->
  <script src="{{asset("global/js/config/colors.js")}}"></script>
  <script src="{{asset("assets/js/config/tour.js")}}"></script>
  <script>
  Config.set('assets', 'assets');
  </script>
  <!-- Page -->
  <script src="{{asset("assets/js/Site.js")}}"></script>
  <script src="{{asset("global/js/Plugin/asscrollable.js")}}"></script>
  <script src="{{asset("global/js/Plugin/slidepanel.js")}}"></script>
  <script src="{{asset("global/js/Plugin/switchery.js")}}"></script>
  <script src="{{asset("global/js/Plugin/matchheight.js")}}"></script>
  <script src="{{asset("global/js/Plugin/jvectormap.js")}}"></script>
  <script src="{{asset("global/js/Plugin/peity.js")}}"></script>
  <script src="{{asset("assets/examples/js/dashboard/v1.js")}}"></script>
  @yield('js')
  <script type="text/javascript">
    function accessPermission(){
      Notification.requestPermission().then(function(result) {
        if (result == "granted") {
          alert('Permission granted.\n     Thank You');
        }
        if (result == "denied") {
          alert('Please enable notification permission for receving notification');
        }
      });
    }
  </script>
  <script type="text/javascript">
function showNotification(title,message,link){
        var notification = new Notification(title,{
          body:message,
          icon:'https://cdn2.iconfinder.com/data/icons/flat-seo-web-ikooni/128/flat_seo3-12-512.png',
          dir:'auto'
        });
        window.NOTIFICATION = notification;
        notification.onclick = function(event) {
          event.preventDefault(); // prevent the browser from focusing the Notification's tab
          window.open(link, '_blank');
          notification.close();
        },
        notification.onClose = function(event,a){
          console.log(event,a);
        }
        setTimeout(function () {
          notification.close();
          }, 9000);
}
  </script>
  <script type="text/javascript">
    function checkNotifctionThings(){
      $.ajax('{{route('checkStatus')}}', {
            type: 'POST',
            data: {
              '_token':"{{csrf_token()}}"
            },
            success: function (data, status, xhr) {
                if (data.vehicleBooking > 0) {
                  showNotification('New Vehicle Booking','we have '+ data.vehicleBooking + ' new vehicle Bookings.','https://www.bsahab.com/admin/vehicleBooking');
                }
                if (data.pickAndDrop > 0) {
                  showNotification('New Pick and Drop Message','we have '+ data.pickAndDrop + ' new pick and drop.','https://www.bsahab.com/admin/pickDrop');
                }
                if (data.userMessage > 0) {
                  showNotification('New User Message','we have '+ data.userMessage + ' new messages.','https://www.bsahab.com/admin/userMessages');
                }
            },
            error: function (jqXhr, textStatus, errorMessage) {
                    console.log('Error' + errorMessage);
                }
        });
    }
    checkNotifctionThings();
    // setInterval(function () {
    //     // checkNotifctionThings();
    // }, 20000);
    // 300000
  </script>
  @yield('script')

</body>
</html>
