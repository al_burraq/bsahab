<!DOCTYPE HTML>
<html>

<head>
  @php

  $URL = $_SERVER['REQUEST_URI'];
  if (!DB::table('seo')->where('url','=',$URL)->exists()) {
    // DB::table('seo')->insert([
    //   'url'=>$_SERVER['REQUEST_URI']
    // ]);
    $title = "Rent a Car | Economy Car Rental Deals | Car for Rent";
    $desciption = "Rent a Car | Economy Car Rental Deals | Car for Rent";
  }else{
    $seo = DB::table('seo')->where('url','=',$URL)->first();
    $title = $seo->title;
    $desciption = $seo->desciption;
    if (strlen($title) < 2) {
      $title = "Rent a Car | Economy Car Rental Deals | Car for Rent";
    }
  }

  @endphp
    <title>{{ $title }}</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="rent a car near me, rent a car in Lahore, discount car rental, rental a car deals, Car for Rent in Lahore, Car Rental, economy car rental, best car rental deals, rent a car" />
    <meta name="description" content="{{$desciption}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="img/bsahablogo1.ico">

    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->

    <link   rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
    <link   rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.css')}}">
    <link   rel="stylesheet" type="text/css" href="{{asset('css/icomoon.css')}}">
    <link   rel="stylesheet" type="text/css" href="{{asset('css/stylesOld.css')}}">
    <link    rel="stylesheet" type="text/css" href="{{asset('css/mystyles.css')}}">
    <script async defer src="{{asset('js/modernizr.js')}}"></script>
    <script   src="{{asset('js/jquery.js')}}"></script>
    <script async defer  type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBEm55Pka9R8WSwCqCnh9GJg30PIxjKljg&libraries=places"></script>
    <script type="text/javascript">
      window.csrf = "{{csrf_token()}}";
    </script>
    <style media="screen">
      .loyalty-card{
        background-color: #6ABE80;
        color: #fff !important;
      }
      .fotorama__stage{
        background: #dddddd21;
      }
      .overlay {


          left: 15px;
      }
    </style>
    <script type="text/javascript">
      function dump(a){
        console.debug(a);
      }
    </script>
    {{-- <script type="text/javascript">
      dump("{{url()->current()}}");
    </script> --}}



     <meta name="robots" content="index, follow">
     <meta name=”geo.placename” content="Lahore" />
     <!-- Global site tag (gtag.js) - Google Analytics -->
     <script async src="https://www.googletagmanager.com/gtag/js?id=UA-98127717-1"></script>
     <script>
       window.dataLayer = window.dataLayer || [];
       function gtag(){dataLayer.push(arguments);}
       gtag('js', new Date());

       gtag('config', 'UA-98127717-1');
     </script>

</head>

<body>

    <div class="global-wrap">
        <header id="main-header">
            <div class="container">
                <div class="nav">
                    <div class="pull-left">
                      <a class="logo" style="margin-right: 14px;" href="{{route('home')}}">
                          <img class="img-responsive" style="width: 184px;display:inline;    margin: 9px 0px;" src="{{asset('img/bsahablogo1.png')}}" alt="Bhai Sahab" title="Bhai Sahab Transport - Book Rental Cars in Lahore">
                      </a>
                    </div>


                    <div class="pull-right" style="margin-top: 17px;">


                    <ul class="slimmenu" id="slimmenu">

                        <li @if (Route::currentRouteName() == "home" || Route::currentRouteName() == "ahome") class="active" @endif>
                          <a href="{{route('home')}}">Home</a>
                        </li>
                        <li class="slimmenu-sub-menu @if (Route::currentRouteName() == "rent-a-Car" || Route::currentRouteName() == "tour-management" || Route::currentRouteName() == "pick-and-drop" || Route::currentRouteName() == "services.index" ) active @endif" >
                          <a href="{{route('services.index')}}">Services</a>
                          <ul>
                               <li><a href="{{route('rent-a-Car')}}">Rent a Car</a>
                               </li>
                               <li><a href="{{route('tour-management')}}">Tour Management</a>
                               </li>
                               <li><a href="{{route('pick-and-drop')}}">Pick and Drop</a>
                               </li>
                           </ul>
                        </li>
                        <li @if (Route::currentRouteName() == "rental-vehicles.index") class="active" @endif>
                          <a href="{{route('rental-vehicles.index')}}">Vehicles</a>
                        </li>
                        <li @if (Route::currentRouteName() == "provinces") class="active" @endif>
                          <a href="{{route('provinces')}}">Destination</a>
                        </li>
                        <li @if (Route::currentRouteName() == "plan-my-tour.index") class="active" @endif>
                          <a href="{{route('plan-my-tour.index')}}">Plan My Tour</a>
                        </li>
                        <li @if (Route::currentRouteName() == "contact-us") class="active" @endif>
                          <a href="{{route('contact-us')}}">Contact us</a>
                        </li>
                        <li >
                          <a href="https://www.bsahab.com/blog">BLOG</a>
                        </li>
                        <li @if (Route::currentRouteName() == "loyalty-card") class="active" @endif>
                          <a class="loyalty-card " href="{{route('loyalty-card')}}">Loyalty Card</a>
                        </li>




                    </ul>
                    </div>
                </div>
            </div>
        </header>

        @yield('body')


        <footer id="main-footer">
            <div class="container">
                <div class="row row-wrap">
                    <div class="col-md-3">
                        <a class="logo" href="{{route('home')}}">
                            <img class="img-responsive" style="width: 184px;display:inline;" src="{{asset('img/bsahablogo1.png')}}" alt="Bhai Sahab" title="Bhai Sahab Transport - Book Rental Cars in Lahore">
                        </a>
                        <p class="mb20">"Explore Pakistan with comfort" <br>`We Offer you Best Rental Vehicles at cheapest rates. Over the last fourteen years, we've become the local favorite by offering a wide selection.`</p>
                        <ul class="list list-horizontal list-space">
                            <li>
                                <a class="fa fa-facebook box-icon-normal round animate-icon-bottom-to-top" href="http://fb.com/bhaisahabtrans"></a>
                            </li>
                            {{-- <li>
                                <a class="fa fa-twitter box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li> --}}
                            {{-- <li>
                                <a class="fa fa-google-plus box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li> --}}
                            <li>
                                <a class="fa fa- fa-instagram box-icon-normal round animate-icon-bottom-to-top" href="https://www.instagram.com/bhai_sahab_transport"></a>
                            </li>
                            {{-- <li>
                                <a class="fa fa-pinterest box-icon-normal round animate-icon-bottom-to-top" href="#"></a>
                            </li> --}}
                        </ul>
                    </div>

                    <div class="col-md-3 col-md-offset-1" >
                        <h4>Newsletter</h4>
                        <form>
                            <label>Enter your E-mail Address</label>
                            <input id="newsLetter" type="text" class="form-control">
                            <p class="mt5"><small>*We Never Send Spam</small>
                            </p>
                            <input type="submit" id="newsLetterBtn" class="btn btn-primary" value="Subscribe">
                        </form>
                    </div>

                    <div class="col-md-4 pull-right">
                        <h4>Have Questions?</h4>

                        <h4 class="text-color" itemprop="telephone">+92-320-440-0993‬</h4>
                        <h4 itemprop="email"><a href="mailto:bhaisahabtrans@gmail.com" class="text-color">bhaisahabtrans@gmail.com</a></h4>
                        <h4><a class="rental_policies" href="https://www.bsahab.com/policy">Rental Policies</a></h4>
                        <h4><a class="rental_policies" href="https://www.bsahab.com/faq">FAQ'S</a></h4>
                    </div>

                </div>

            </div>
        </footer>
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center">
              Design and Develop by
              <a href="https://www.al-burraq.com/">Al-Burraq Technologies</a>
            </div>
          </div>
        </div>


        <script async defer src="{{asset('js/bootstrap.js')}}"></script>
        <script type="text/javascript">

           function magic(){
             // console.log('magic');
             $.getScript("https://www.bsahab.com/js/custom.js");

           }

        </script>

        <script async src="{{asset('js/slimmenu.js')}}"></script>
        <script async src="{{asset('js/bootstrap-datepicker.js')}}"></script>
        <script async src="{{asset('js/bootstrap-timepicker.js')}}"></script>
        <script async src="{{asset('js/nicescroll.js')}}"></script>
        <script async src="{{asset('js/dropit.js')}}"></script>
        <script async src="{{asset('js/ionrangeslider.js')}}"></script>
        <script async src="{{asset('js/icheck.js')}}"></script>
        <script async src="{{asset('js/fotorama.js')}}"></script>
        <script onload="magic()" async src="{{asset('js/jquery.geocomplete.js')}}"></script>
        <script async src="{{asset('js/typeahead.js')}}"></script>
        <script async src="{{asset('js/card-payment.js')}}"></script>
        <script async src="{{asset('js/magnific.js')}}"></script>
        <script async src="{{asset('js/owl-carousel.js')}}"></script>
        <script async src="{{asset('js/fitvids.js')}}"></script>
        {{-- <script src="{{asset('js/tweet.js')}}"></script> --}}
        {{-- <script src="{{asset('js/countdown.js')}}"></script> --}}
        <script async  src="{{asset('js/gridrotator.js')}}"></script>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header" id="headingofModel" style="display:none">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 id="modalHeading" class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p id="modalMessage" style="font-size: 25px;
    line-height: 41px;" class="text-center">This is a small modal.</p>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }
    $("#newsLetterBtn").click(function(event) {
      event.preventDefault();
      email = $("#newsLetter").val();
      if (!validateEmail(email)) {
        notify('validation',"<strong>"+email + "</strong> is not a valid email.<br>Please Enter a valid Email");
        return 0;
      }else{
        $.ajax({
          url: '{{route('subscribNewLetter')}}',
          type: 'POST',
          data: {
            _token: window.csrf,
            email : email
          }
        })
        .done(function(data) {
          if (data == "noEmail") {
            notify("Error","Email is not given.Please try again");
            return 0;
          }
          if (data == "invalidEmail") {
            notify("Error","Email is not valid.Please try again");
            return 0;
          }
          if (data == "true") {
            notify('Informations','News Letter subscribed successfully<br>Thank you');
            return 0;
          }
          if (data == "exists") {
            notify('Information','Email already subscribed');
            return 0;
          }

        })
        .fail(function() {
          // notify('Error','Error please try again.<br><br>Are you connected to internet ?');
        })
        .always(function() {

        });

      }


    });
  </script>
</body>

</html>
