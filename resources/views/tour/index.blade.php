@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')

  <div class="full-r"><img src="{{asset("img/plan-your-tour-to-kpk.jpg")}}" alt="Rental Car Services - Pick and Drop Services - Bhai Sahab transport" ></div>

  <div class="container marginb15">

  	<div class="row">
  		<div class="container" style="margin-bottom:120px;">
              <h1 class="booking-title">Plan Your Tour - Transport Expense</h1>
              <div class="row">
                  <div class="col-sm-4 col-md-3 col-xs-12" style="margin-top:20px;">
                      <aside class="plan_tour sidebar-left">
                          <form action="redirect.php?" method="get">
                              <div class="form-group form-group-icon-left">
                                  <label class="label-nav">Select Vehicle</label>
                                  <input type="hidden" name="page" value="plan-my-tour">
<select name="tour_vehicle" class="form-control form-control-2" id="vehicle">
  @foreach ($vehicles as $vehicle)
    <option value="{{$vehicle->id}}">{{trim($vehicle->vehicle_make)}} {{trim($vehicle->name)}}</option>
  @endforeach
</select>
                              </div>
                              <div class="form-group form-group-icon-left">
                              <label class="label-nav">From</label>
                                 <input class="typeahead form-control form-control-2" id="pickup-address" placeholder="City, Airport, U.S. Zip" type="text" />

                          </div>
                              <div class="form-group form-group-icon-left">
                              <label class="label-nav">Select Destination</label>
                                 <select name="tour_list" id="destination" class="form-control form-control-2">
@foreach ($destinations as $destination)
  <option value="{{$destination->destination_id}}">{{$destination->title}}</option>
@endforeach
                        </select>

                          </div>
                              <div class="form-group form-group-icon-left">
                              <label class="label-nav">Please Enter Your Budget</label>
                        <input class="form-control form-control-2" id="buget" type="number"  min="1" onkeypress="return isNumber(event)" name="tour_budget" required
                         value="">
                          </div>
                              <button class="btn btn-primary mt10-1" name="plan_tour" id="plan_tourBTn" type="button" name="button">Search</button>
                          </form>
                      </aside>
                  </div>
                        <div class="col-sm-8 col-md-9 col-xs-12" id="vehcileDiv" style="display:none;">
                          <div class="booking-item" itemscope="" itemtype="https://auto.schema.org/Vehicle" style="margin-top:20px;">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="booking-item-car-img">
<a id="linkSource" href="vehicle/129" itemprop="mainEntityOfPage"><img style="

border: 1px solid #BFBFBF;
background-color: white;
box-shadow: 2px 2px 1px #aaaaaa;" id="vehcileImage" src="img/loadingimg.gif" itemprop="image"></a>
                                                <p class="booking-item-car-title" itemprop="name" id="vehcileName"></p>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="row">
                                                <div class="col-md-12">
<ul class="booking-item-features booking-item-features-sign clearfix">
<li rel="tooltip" data-placement="top" title="" data-original-title="Passengers"><i class="fa fa-male"></i><span class="booking-item-feature-sign" itemprop="vehicleSeatingCapacity" id="vehicleSeatingCapacity">x 7</span>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="Doors"><i class="im im-car-doors"></i><span class="booking-item-feature-sign" itemprop="numberOfDoors" id="numberOfDoors">x 4</span>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="Baggage Quantity"><i class="fa fa-briefcase"></i><span class="booking-item-feature-sign" itemprop="cargoVolume" id="cargoVolume">x 4</span>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="Automatic Transmission"><i class="im im-shift-auto"></i><span class="booking-item-feature-sign" itemprop="vehicleTransmission" id="vehicleTransmission">auto</span>
</li>

<li style="margin-left: 5px;" rel="tooltip" data-placement="top" title="" data-original-title="Hybrid Vehicle"><i class="im im-diesel"></i><span class="booking-item-feature-sign" itemprop="vehicleTransmission" id="engine">hybrid</span>
</li>
</ul>
                                                </div>
                                                <div class="col-md-12">
<ul class="booking-item-features booking-item-features-small clearfix">
<li rel="tooltip" data-placement="top" title="" data-original-title="Climate Control"><i class="im im-climate-control" id="im-climate-control"></i>
</li>
<li rel="tooltip" data-placement="top" title="" data-original-title="Air Conditioning" id="AirConditioning"><i class="im im-air"></i>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="Satellite Navigation" id="im-satellite">
<i class="im im-satellite"></i>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="Power Door Locks"><i class="im im-lock" id="im-lock"></i>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="LCD" id="lcd">
<i class="fa fa-desktop" aria-hidden="true"></i>
</li>

<li rel="tooltip" data-placement="top" title="" data-original-title="Tilt Steering Wheel" id="steeringwheel">
<i class="im im-car-wheel"></i>
</li>
</ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price" id="itemPrice">Rs.8500</span><span>/day</span>
                                            <a id="carLink" href="rental-vehicle-detail/129"><p class="booking-item-flight-class">
                                                    Premium</p><span class="btn btn-primary">Select</span></a>
                                        </div>
                                    </div>



                                </div>
                        </div>

                          <div id="destinationDiv" class="row row-wrap" style="display:none;border:1px solid #e6e6e6;padding:40px;margin:0;margin-bottom:20px">


        <div class="col-md-5 widthh" style="margin-top:20px;">
            <div class="thumb thumb_img">
                <header class="thumb-header">
                    <a class="hover-img curved" href="tourist-attraction-detail/58">
                        <img style=" padding: 10px 10px 20px 10px;
    border: 1px solid #BFBFBF;
    background-color: white;
    box-shadow: 10px 10px 5px #aaaaaa;" id="destinationImage" src="img/loadingimg.gif" alt="Image Alternative text" ><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
                    </a>
                </header>
                <div class="thumb-caption">
                    <h4 class="thumb-title" id="destinationTitle">Jehlum</h4>
                    <p class="thumb-desc" id="destinationDesc">The Mangla Dam (Urdu: ????? ????) is a multipurpose dam located on the Jhelum River in the Mirpur District of Azad Kashmir, Pakistan. It is the 7th largest dam in the world. The project was designed and supervised by Binnie &amp; Partners of London (the team led by partner Geoffrey Binnie),[2] and it was built by Mangla Dam Contractors, a consortium of 8 U.S. construction firms, sponsored by Guy F. Atkinson Company of South San Francisco.																				</p>
                </div>
            </div>
        </div>

        <div class="col-md-4 detail" id="tourDetails" style="margin-top:20px;">
                                                    Total Days: 256 <br>
            Tour Travel Budget: Rs.1770 <br>
            Total Distance: 177km <br>

        </div>
        <div class="clearfix"></div>



</div>
  			                <div id="oldMessageDiv" class="col-sm-8 col-md-9 col-xs-12" style="margin-top:20px;">
                      <h3>Instructions</h3>
                      <ul>
                          <li>Choose Your vehicle</li>
                          <li>Select Your destination</li>
                          <li>Let us know your budget</li>
                      </ul>
                      <p>
                          We will give you your estimate expense to travel at that location. This expense does not include your hotel and food expenditures.
                          It includes vehicle rent and fuel consumption expense based on the vehicle you choose.
                      </p>
                      <div class="col-md-12">
                          <a class="center-me btn btn-primary" href="contact-us">Contact us for more details</a>
                      </div>
                  </div>
                          </div>
          </div>
  	</div>
  </div>



<script type="text/javascript">
$(document).ready(function(){

function getDistance(){
  destination = $("#destination option:selected").text();
  from        = $("#pickup-address").val();
  var origin = from,
  destination = destination,
  service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix(
  {
      origins: [origin],
      destinations: [destination],
      travelMode: google.maps.TravelMode.DRIVING,
      avoidHighways: false,
      avoidTolls: false
  },
  function(response){
    window.response = response;
    newDestination = response.destinationAddresses[0];
    newOrign = response.originAddresses[0];
    distvalue = response.rows[0].elements[0].distance.text;

    destination = $("#destination").val();
    from        = $("#pickup-address");
    vehicle     = $("#vehicle").val();
    buget       = $("#buget").val();

    $.ajax({
      url: '{{route('planeTour')}}',
      type: 'POST',
      data: {
        vehicle    : vehicle,
        destination: destination,
        buget      : buget,
        _token     : "{{csrf_token()}}",
        distance   : distvalue
      }
    }).then(function(response){
      response = JSON.parse(response);
      console.log(response);
      vehcile = response.vehcile;
      destination = response.destination;
      if (response.status == 0) {
        $("#vehcileDiv").hide();
        $("#destinationDiv").hide();
        $("#oldMessageDiv").show();

        notify('Information',response.message);
      }
      if (response.status == 1) {
        $("#oldMessageDiv").hide();

        imagePath = 'img/loadingimg.gif';
        $("#vehcileImage").attr('src',imagePath);
        $("#destinationImage").attr('src',imagePath);
        $("#vehcileDiv").show();
        $("#destinationDiv").show();

        if (destination) {
          imagePath = 'img/'+destination.image;
          $("#destinationImage").attr('src',imagePath);
          $("#destinationTitle").html(destination.title);
          $("#destinationDesc").html(destination.description);
          htmlA = "Total Days: "+ response.days+"<br>Tour Travel Budget: Rs."+Math.round(response.travelBuget)+"<br>Total Distance:"+distvalue+" <br>"
          $("#tourDetails").html(htmlA);
        }

        if (vehcile) {
          imagePath = 'img/'+vehcile.id+'/'+vehcile.image;
          $("#vehcileImage").attr('src',imagePath);
        }
        var vehicleName = vehcile.name;
        console.log(vehicleName);
        var newLink = "vehicle/"+vehcile.id+"/"+vehicleName.replace(/\s/g, '');
        $("#linkSource").attr('href',newLink);
        console.log(newLink);
        $("#carLink").attr('href',newLink);
        if (vehcile.air_conditioning == 'yes') {
          $("#AirConditioning").show();
        }else{
          $("#AirConditioning").hide();
        }
        $("#vehicleSeatingCapacity").html("x "+vehcile.passenger);
        $("#numberOfDoors").html("x "+vehcile.doors);
        $("#cargoVolume").html("x "+vehcile.baggage_quantity);
        $("#vehicleTransmission").html(vehcile.manual_transmission);
        $("#engine").html(vehcile.engine);
        if (vehcile.climate_controll == 'yes') {
          $("#im-climate-control").show();
        }else{
          $("#im-climate-control").hide();
        }
        if (vehcile.air_conditioning == 'yes') {
          $("#AirConditioning").show();
        }else{
          $("#AirConditioning").hide();
        }
        if (vehcile.satellite_navigation == 'yes') {
          $("#im-satellite").show();
        }else{
          $("#im-satellite").hide();
        }
        if (vehcile.power_door_locks == 'yes') {
          $("#im-lock").show();
        }else{
          $("#im-lock").hide();
        }
        if (vehcile.lcd == 'yes') {
          $("#lcd").show();
        }else{
          $("#lcd").hide();
        }
        if (vehcile.tilt_steering_wheel == 'yes') {
          $("#steeringwheel").show();
        }else{
          $("#steeringwheel").hide();
        }
        $("#itemPrice").html("Rs."+vehcile.per_day_rate);




      }


    }, function(error){
      console.log(error);
    });
  }

  );
}

$("#plan_tourBTn").click(function(event) {
  destination = $("#destination").val();
  from        = $("#pickup-address").val();
  vehicle     = $("#vehicle").val();
  buget       = $("#buget").val();

  if (buget == null || buget.length < 1) {
    notify('error','Please Enter buget and try again');
    return 0;
  }
  if (from == null || from.length < 3) {
    notify('error','Please Enter from Location and try again');
    return 0;
  }
  console.log(from);
  getDistance();

});
function planeTourAjax(response,status){
  destination = $("#destination").val();
  from        = $("#pickup-address");
  vehicle     = $("#vehicle").val();
  buget       = $("#buget").val();

  $.ajax({
    url: '{{route('planeTour')}}',
    type: 'POST',
    data: {
      vehicle    : vehicle,
      destination: destination,
      buget      : buget,
      _token     : "{{csrf_token()}}"
    }
  }).then(function(response){
    console.log(response);
  }, function(){

  });
}
});
// docuemtn ready ends here





function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}

</script>

@endsection
