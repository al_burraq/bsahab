@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')

  <div class="container">
              <h1 class="page-title">Contact us</h1>
  </div>
  <div class="row">
     <div id="map-canvas" style="height:400px;"></div>

  </div>
<div class="container">
  <div class="gap"></div>
            <div class="row">
                <div class="col-md-7">
                    <p>We provide you services for pick and drop and rent vehicles for your tour.
                        Contact us for you customized tours. We have tour guides available for Urdu and English.</p>
                    <p>If you are coming from outside Pakistan we can arrange party time for you. We can make
                        bestarrangements for you to explore Pakistan.</p>
                    <form class="mt30" action="" method="post" onsubmit="return emailValidation();">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" id="name" type="text" name="name" required="required"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>E-mail</label>
                                    <input class="form-control" id="email" type="email" name="email" required="required" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" id="message" name="message"></textarea>
                        </div>
                        <input class="btn btn-primary" id="sendMessage" type="submit" name="submit_message" value="Send Message" />
                    </form>
                </div>
                <div class="col-md-4" itemscope itemtype="http://schema.org/AutoRental">
                    <aside class="sidebar-right">
                        <ul class="address-list list">
						                            <li>
                                <h5>Email</h5><a href="#" itemprop="email">bhaisahabtrans@gmail.com</a>
                            </li>
                            <li>
                                <h5>Phone Number</h5><a href="#" itemprop="telephone">+92-320-440-0993‬</a>
                            </li>
                            <li>
                                <h5>Address</h5><address itemprop="address">49-C 2nd Floor Commercial Area Cavalry Ground Lahore Cantt.</address>
                            </li>
							                        </ul>
                    </aside>
                </div>
            </div>
            <div class="gap"></div>
        </div>

</div>
<script type="text/javascript">
  $("#sendMessage").click(function(event) {

    event.preventDefault();
    name = $("#name").val();
    email = $("#email").val();
    message = $("#message").val();

    if (name < 3) {
      notify("Error","Please Enter a valid Name");
      return 0;
    }
    if (email < 3) {
      notify("Error","Please Enter a valid email");
      return 0;
    }
    if (message < 3) {
      notify("Error","Please enter a valid messge");
      return 0;
    }

    if (!validateEmail(email)) {
      notify('validation',"<strong>"+email + "</strong> is not a valid email.<br>Please Enter a valid Email");
      return 0;
    }
    $.ajax({
      url: '{{route('userMessageSave')}}',
      type: 'POST',
      data: {
        _token: window.csrf,
        name: name,
        email : email,
        message: message
      }
    })
    .done(function(data) {
      if (data == "noName") {
        notify("Error","Name is not given.Please try again");
      }
      if (data == "noMessage") {
        notify("Error","Message is not given.Please try again");
      }
      if (data == "noEmail") {
        notify("Error","Email is not given.Please try again");
        return 0;
      }
      if (data == "invalidEmail") {
        notify("Error","Email is not valid.Please try again");
        return 0;
      }
      if (data == "true") {
        notify('Informations','Thank you for getting in touch!<br>We have received your message<br>We will contact you shortly.');
        return 0;
      }
    })
    .fail(function() {
      // notify('Error','Error please try again.<br><br>Are you connected to internet ?');
    })
    .always(function() {
      console.log("complete");
    });


  });
</script>
@endsection
