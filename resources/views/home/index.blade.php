@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')
  <!-- TOP AREA -->
  <div class="top-area show-onload">
      <div class="bg-holder full">
          {{-- <div class="bg-mask"></div> --}}
          {{-- <div class="bg-img" style="background-image:url(img/2048x1365.png);"></div> --}}
          <video class="bg-video hidden-sm hidden-xs" preload="auto" autoplay="true" loop="loop" muted="muted"  >
              <source src="media/video.webm" type="video/webm" />
              <source src="media/video.m4v" type="video/m4v" />
          </video>

          <div class="bg-front full-height">
              <div class="container rel full-height">
                  <div class="search-tabs search-tabs-bg search-tabs-bottom">
                      <div class="tabbable">
                          <ul class="nav nav-tabs" id="myTab">
                              <li><a href="#tab-4" data-toggle="tab"><i class="fa fa-car"></i> <span >Vehicle</span></a>
                              </li>
                          </ul>
                          <div class="tab-content">
                              <div class="tab-pane fade in active" id="tab-4">
                                  <h2>Book Cheap Rental Vehicle</h2>
                                  <form>
                                      <div class="row">
                                          <div class="col-md-12">
                                              <div class="row">
                                                  <div class="col-md-4">
                                                      <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                                          <label>Pick-up Location</label>
                                                          <input class="typeahead form-control" id="pickup-address" placeholder="City" type="text" value="Lahore, Pakistan" />
                                                      </div>
                                                  </div>
                                                  <div class="col-md-4">
                                                      <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon"></i>
                                                          <label>Drop-off Location</label>
                                                          <input class="typeahead form-control" id="drop-off-address" placeholder="City" type="text" />
                                                      </div>
                                                  </div>
                                                  <div class="col-md-2">
                                                      <div class="form-group form-group-lg">
                                                          <label>Choose Vehicle</label>
                                                          <select class="form-control" id="vehicle" name="vehicle">
                                                            @foreach ($vehicles as $vehicle)
                                                              <option value="{{$vehicle->id}}">{{$vehicle->vehicle_make}} {{$vehicle->name}}</option>
                                                            @endforeach
                                                          </select>
                                                      </div>
                                                  </div>
                                                  <div class="col-md-2">
                                                      <div class="form-group form-group-lg">
                                                          <label>Phone No</label>
                                                          <input class="form-control" id="phoneNo" placeholder="Phone No" type="text" />
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-md-12">
                                              <div class="input-daterange" data-date-format="M d, D">
                                                  <div class="row">
                                                      <div class="col-md-6">
                                                          <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                              <label>Pick-up Date</label>
                                                              <input class="form-control" id="pickDate" name="start" type="text" />
                                                          </div>
                                                      </div>
                                                      <div class="col-md-6">
                                                          <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                              <label>Drop-off Date</label>
                                                              <input class="form-control" id="dropDate" name="end" type="text" />
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <button class="btn btn-primary btn-lg" id="bookVehicle" type="submit">Book Vehicle</button>
                                  </form>
                              </div>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- END TOP AREA  -->

  <div class="gap"></div>

  <div class="container">
    <div class="col-md-12 text-center">
          <h1>Bhai Sahab Transport - Rent A Car</h1><br>
    </div>
      <div class="text-center row">
          <div class="col-md-8 col-md-offset-2">
              <div class="row row-wrap" data-gutter="60">
                  <div class="col-md-4">
                      <div class="thumb">
                          <header class="thumb-header"><i class="fa fa-dollar box-icon-gray box-icon-center round box-icon-border box-icon-big animate-icon-top-to-bottom"></i>
                          </header>
                          <div class="thumb-caption">
                              <h5 class="thumb-title"><a class="text-darken" href="#">Best Price Guarantee</a></h5>
                              <p class="thumb-desc">You can find the best car rental prices at our website. We have selected best cars from the whole </p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="thumb">
                          <header class="thumb-header"><i class="fa fa-lock box-icon-gray box-icon-center round box-icon-border box-icon-big animate-icon-top-to-bottom"></i>
                          </header>
                          <div class="thumb-caption">
                              <h5 class="thumb-title"><a class="text-darken" href="#">Trust & Safety</a></h5>
                              <p class="thumb-desc">You can trust our staff, every one is slected after a tough scrutiny. Your trust is our confidence.</p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-4">
                      <div class="thumb">
                          <header class="thumb-header"><i class="fa fa-send box-icon-gray box-icon-center round box-icon-border box-icon-big animate-icon-top-to-bottom"></i>
                          </header>
                          <div class="thumb-caption">
                              <h5 class="thumb-title"><a class="text-darken" href="#">Best Destinations</a></h5>
                              <p class="thumb-desc">Our Staff is trained and can help you out in selecting the best Destinations for your vacations.</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="gap gap-small"></div>
  </div>
  <div class="bg-holder">
      <div class="bg-mask"></div>
      <div class="bg-blur" style="background-image:url({{asset("img/road.jpg")}});"></div>
      <div class="bg-content">
          <div class="container">
              <div class="gap gap-big text-center text-white">
                  <h2 class="text-uc mb20">BEST DESTINATION</h2>
                  <ul class="icon-list list-inline-block mb0 last-minute-rating">
                      <li><i class="fa fa-star"></i>
                      </li>
                      <li><i class="fa fa-star"></i>
                      </li>
                      <li><i class="fa fa-star"></i>
                      </li>
                      <li><i class="fa fa-star"></i>
                      </li>
                      <li><i class="fa fa-star"></i>
                      </li>
                  </ul>
                  <h5 class="last-minute-title">{{$bestDastination->title}}</h5>

                  <p class="mb20"><a class="btn btn-lg btn-white btn-ghost" href="{{route('destination',$bestDastination->destination_id)}}/{{$bestDastination->title}}">View Destination<i class="fa fa-angle-right"></i></a>
              </div>
          </div>
      </div>
  </div>
  <div class="container">
      <div class="gap"></div>
      <h2 class="text-center">Top Destinations to Spend Vacations</h2>
      <div class="gap">
          <div class="row row-wrap">
              @foreach ($destinations as $destination)
              <div class="col-md-3">
                  <div class="thumb">
                      <header class="thumb-header">
                          <a class="hover-img curved" style="min-height: 150px;max-height: 150px;" href="{{route('destination',$destination->destination_id)}}/{{$destination->title}}">
                              <img src="{{asset("img/thumbnail/".$destination->image)}}" alt="{{$destination->discription}}" title="Upper Lake in New York Central Park" /><i class=" box-icon-white box-icon-border hover-icon-top-right round"></i>
                          </a>
                      </header>
                      <div class="thumb-caption">
                          <h4 class="thumb-title">
                              {{$destination->title}}</h4>
                          <p class="thumb-desc">{{$destination->description}}</p>
                      </div>
                  </div>
              </div>
              @endforeach

          </div>
      </div>


  </div>
<script type="text/javascript">
  $("#bookVehicle").click(function(event) {
    event.preventDefault();

    pickupAddress = $("#pickup-address").val();
    dropOffAddress = $("#drop-off-address").val();
    vehicle  = $("#vehicle option:selected").text();

    phoneNo = $("#phoneNo").val();
    pickUpDate = $("#pickDate").val();
    dropDate   = $("#dropDate").val();


    if (pickupAddress.length < 2) {
      notify('Information','Please Choice Pick up Address');
      return 0;
    }
    if (dropOffAddress.length < 2) {
      notify('Information','Please Choice drop of address');
      return 0;
    }
    if (vehicle.length < 2) {
      notify('Information','Please Choice vehicle');
      return 0;
    }

    if (phoneNo.length < 6) {
      notify('Information','Please Enter a valid phone Number');
      return 0;
    }
    if (pickUpDate.length < 2) {
      notify('Information','Please Choice Pick up date');
      return 0;
    }
    if (dropDate.length < 2) {
      notify('Information','Please Choice drop of date');
      return 0;
    }

    $.ajax({
      url: '{{route('car-Booking')}}',
      type: 'POST',
      data: {
        _token: window.csrf,
        pickLocation: pickupAddress,
        dropLocation: dropOffAddress,
        car: vehicle,
        phon: phoneNo,
        pickDate: pickUpDate,
        dropDate:dropDate
    }
    })
    .done(function(data) {
      if (data == "emptyfields") {
        notify('You have leave some field Empty');
        return 0;
      }
      if (data == "true") {
        notify('success',' Your Ride is Successfully Booked!<br> We will contact you shortly.');
        $("#pickup-address").val('');
        $("#drop-off-address").val('');
        $("#phoneNo").val('');

      }else{
        notify('Error','There was error durring Vechile Booking');
      }

    })
    .fail(function() {
      notify('Error','Error please try again');
    })
    .always(function() {

    });

  });
</script>
@endsection
