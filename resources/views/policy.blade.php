@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')


  <div class="full-r">
      <img src="{{asset('img/Terms-and-Conditions.jpg')}}" alt="Terms and Conditions" title="Terms and Conditions">
  </div>
  <div class="container" >
  <h1 style="booking-title">TERMS & CONDITIONS</h1>
  <ul >
    <li>You can get your vehicle at maximum 2 hours before the booking date.</li>
    <li>Overtime will be charged on the basis hours.(Hourly Price charged, vary from vehicle to vehicle)</li>
    <li>Challan other than company officials mistake will be charged to customers common example is route unavailability in KPK (There is no such regulation exists).</li>
    <li>Driver’s Accommodation/Food will be customer’s responsibility.</li>
    <li>Company will not be responsible for God forbid any accident.</li>
    <li>In case you have not hired driver, Vehicle will be your responsibility.</li>
    <li>You need to submit your CNIC photocopy to the company officials before you leave with vehicle.</li>
    <li>Vehicle will be given after proper verification.</li>
    <li>You need to submit at least 2 days rent for advance bookings.</li>
    <li>In case any problems instead of any hot debate with our staff Contact complaint cell directly.Phone: 0313-3359533</li>
  </ul>


  <h4>1.1	CANCELLATION POLICY.</h4>
  <ul>
    <li>-	72 Hours , 25% of cost  will be deducted</li>
    <li>-	48 Hours , 50%  of cost will be deducted</li>
    <li>-	24 Hours , 100% of will be deducted</li>
  </ul>
</div>


@endsection
