@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')
<style type="text/css">
     #map {
        height: 350px;
		margin-bottom:50px;
      }
	  .iw-content img
	  {
		  width:70px !important;
	  }

#map img {
	max-width: none !important;
}
.gm-style-iw {


	left:21px !important;

}

#iw-container .iw-title {
	font-family: 'Open Sans Condensed', sans-serif;
	font-size: 22px;
	font-weight: 400;
	padding: 10px;
	background-color: #48b5e9;
	color: white;
	margin: 0;
	border-radius: 2px 2px 0 0;
}

.iw-content img {
	float: right;
	margin: 0 5px 5px 10px;
}
.iw-subTitle {
	font-size: 16px;
	font-weight: 700;
	padding: 5px 0;
}
.iw-bottom-gradient {
	position: absolute;
	width: 326px;
	height: 25px;
	bottom: 10px;
	right: 18px;
	background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
}

.detinationImage {
  z-index: 1;
}
.overlay {
    position: absolute;
    top: 0;
    height: 173px;
    width: 90%;
  	z-index: 3;
    opacity: 0.5;
    left: 15px;
}
</style>

<div id="map"></div>

<div class="container">

</div>

<script type="text/javascript">
var map;
  var latlng = new google.maps.LatLng(30.0706, 71.0937);
  var myOptions = {
  zoom: 5,
  center: latlng,
  draggable: false,
  mapTypeId: google.maps.MapTypeId.ROADMAP,
};
map = new google.maps.Map(document.getElementById("map"), myOptions);
@foreach ($destinations as $destination)
var marker = new google.maps.Marker
(
    {
        position: new google.maps.LatLng({{$destination->latitude}},{{$destination->longitude}}),
        map: map,
        title: '{{$destination->title}}',
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    }
);

var content = '<div id="iw-container">' +
'<div class="iw-title">{{$destination->title}}</div>'+
'<div style="float:left; width:50%;"><div class="iw-content">'+
'<div class="iw-subTitle">History</div>'+
'<p>{{preg_replace('/[\r\n]+/','', $destination->description)}}</p>'+
'<br><a href = "{{route('destinations.show',$destination->id)}}">Click Here </a> For more details</p>'+
'</div>' +
'</div>'+
'<div style="float:right; width:47%;">'+
'<img src="{{asset('img/'.$destination->image)}}" alt="$title" height="150px" width="150px">' +
'</div>'+
'</div>';

var infowindow = new google.maps.InfoWindow();
google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
return function() {
  infowindow.setContent(content);
  infowindow.open(map,marker);
};
})(marker,content,infowindow));
@endforeach

</script>
<div class="container">

<div class="row row-wrap">



<div class="col-md-3" >
<a href="{{route('provinceNew',1)}}/punjab"><p style="position: absolute;top: 40%;font-size: 30px;color: #fff;text-align: center;margin: 0 auto;width: 90%;z-index:111;">Punjab</p></a>
  <div class="thumb">
      <header class="thumb-header">
          <a class="" href="{{route('provinceNew',1)}}/punjab" itemprop="mainEntityOfPage">
              <img src="{{asset('img/thumbnail/punjab.png')}}" class="detinationImage" alt="punjab" title="punjab" itemprop="image" height="173">
              <img class="overlay" src="{{asset('img/overlay.png')}}">
          </a>
      </header>
  </div>
</div>
<div class="col-md-3" >
<a href="{{route('provinceNew',2)}}/sindh"><p style="position: absolute;top: 40%;font-size: 30px;color: #fff;text-align: center;margin: 0 auto;width: 90%;z-index:111;">Sindh</p></a>
  <div class="thumb">
      <header class="thumb-header">
          <a class="" href="{{route('provinceNew',2)}}/sindh" itemprop="mainEntityOfPage">
              <img src="{{asset('img/thumbnail/sindh.jpg')}}" class="detinationImage" alt="Sindh" title="Sindh" itemprop="image" height="173">
              <img class="overlay" src="{{asset('img/overlay.png')}}">
          </a>
      </header>
  </div>
</div>
<div class="col-md-3" >
<a href="{{route('provinceNew',3)}}/Khyber-Pakhtunkhwa"><p style="position: absolute;top: 40%;font-size: 30px;color: #fff;text-align: center;margin: 0 auto;width: 90%;z-index:111;">Khyber Pakhtunkhwa</p></a>
  <div class="thumb">
      <header class="thumb-header">
          <a class="" href="{{route('provinceNew',3)}}/Khyber-Pakhtunkhwa" itemprop="mainEntityOfPage">
              <img src="{{asset('img/thumbnail/kpk.jpg')}}" class="detinationImage" alt="Khyber Pakhtunkhwa" title="Khyber Pakhtunkhwa" itemprop="image" height="173">
              <img class="overlay" src="{{asset('img/overlay.png')}}">
          </a>
      </header>
  </div>
</div>
<div class="col-md-3" >
<a href="{{route('provinceNew',4)}}/blochistan"><p style="position: absolute;top: 40%;font-size: 30px;color: #fff;text-align: center;margin: 0 auto;width: 90%;z-index:111;">Blochistan</p></a>
  <div class="thumb">
      <header class="thumb-header">
          <a class="" href="{{route('provinceNew',4)}}/blochistan" itemprop="mainEntityOfPage">
              <img src="{{asset('img/thumbnail/blochistan.jpg')}}" class="detinationImage" alt="blochistan" title="blochistan" itemprop="image" height="173">
              <img class="overlay" src="{{asset('img/overlay.png')}}">
          </a>
      </header>
  </div>
</div>
<div class="col-md-3" >
<a href="{{route('provinceNew',5)}}/Gilgit-Baltistan"><p style="position: absolute;top: 40%;font-size: 30px;color: #fff;text-align: center;margin: 0 auto;width: 90%;z-index:111;">Gilgit-Baltistan</p></a>
  <div class="thumb">
      <header class="thumb-header">
          <a class="" href="{{route('provinceNew',5)}}/Gilgit-Baltistan" itemprop="mainEntityOfPage">
              <img src="{{asset('img/thumbnail/baltistan.jpg')}}" class="detinationImage" alt="Gilgit-Baltistan" title="Gilgit-Baltistan" itemprop="image" height="173">
              <img class="overlay" src="{{asset('img/overlay.png')}}">
          </a>
      </header>
  </div>
</div>
<div class="col-md-3" >
<a href="{{route('provinceNew',6)}}/Azad-kashmir"><p style="position: absolute;top: 40%;font-size: 30px;color: #fff;text-align: center;margin: 0 auto;width: 90%;z-index:111;">Azad kashmir</p></a>
  <div class="thumb">
      <header class="thumb-header">
          <a class="" href="{{route('provinceNew',6)}}/Azad-kashmir" itemprop="mainEntityOfPage">
              <img src="{{asset('img/thumbnail/kashmir.jpg')}}" class="detinationImage" alt="Azad kashmir" title="Azad kashmir" itemprop="image" height="173">
              <img class="overlay" src="{{asset('img/overlay.png')}}">
          </a>
      </header>
  </div>
</div>







	</div>
</div>


@endsection
