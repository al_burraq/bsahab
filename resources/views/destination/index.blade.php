@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')
<style type="text/css">
     #map {
        height: 350px;
		margin-bottom:50px;
      }
	  .iw-content img
	  {
		  width:70px !important;
	  }

#map img {
	max-width: none !important;
}
.gm-style-iw {


	left:21px !important;

}

#iw-container .iw-title {
	font-family: 'Open Sans Condensed', sans-serif;
	font-size: 22px;
	font-weight: 400;
	padding: 10px;
	background-color: #48b5e9;
	color: white;
	margin: 0;
	border-radius: 2px 2px 0 0;
}

.iw-content img {
	float: right;
	margin: 0 5px 5px 10px;
}
.iw-subTitle {
	font-size: 16px;
	font-weight: 700;
	padding: 5px 0;
}
.iw-bottom-gradient {
	position: absolute;
	width: 326px;
	height: 25px;
	bottom: 10px;
	right: 18px;
	background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
}

.detinationImage {
  z-index: 1;
}
.overlay {
    position: absolute;
    top: 0;
    height: 173px;
    width: 90%;
  	z-index: 3;
    opacity: 0.5;
}
</style>

<div id="map"></div>

<div class="container">

</div>

<script type="text/javascript">
$(document).ready(function() {

var map;
  var latlng = new google.maps.LatLng(30.0706, 71.0937);
  var myOptions = {
  zoom: 5,
  center: latlng,
  draggable: false,
  mapTypeId: google.maps.MapTypeId.ROADMAP,
};
map = new google.maps.Map(document.getElementById("map"), myOptions);
@foreach ($destinations as $destination)
var marker = new google.maps.Marker
(
    {
        position: new google.maps.LatLng({{$destination->latitude}},{{$destination->longitude}}),
        map: map,
        title: '{{$destination->title}}',
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    }
);

var content = '<div id="iw-container">' +
'<div class="iw-title">{{$destination->title}}</div>'+
'<div style="float:left; width:50%;"><div class="iw-content">'+
'<div class="iw-subTitle">History</div>'+
'<p>{{preg_replace('/[\r\n]+/','', $destination->description)}}</p>'+
'<br><a href = "{{URL::to('/') .'/tourist-attraction-Detail/'.$destination->destination_id."/".preg_replace('/\s+/', '-',$destination->title)}}">Click Here </a> For more details</p>'+
'</div>' +
'</div>'+
'<div style="float:right; width:47%;">'+
'<img src="@if(file_exists('img/thumbnail/'.$destination->image)){{asset('img/thumbnail/'.$destination->image)}}@else{{asset('img/'.$destination->image)}}@endif" height="150px" width="150px">' +
'</div>'+
'</div>';

var infowindow = new google.maps.InfoWindow();
google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
return function() {
  infowindow.setContent(content);
  infowindow.open(map,marker);
};
})(marker,content,infowindow));
@endforeach
});

</script>
<div class="container">

<div class="row row-wrap">

  @foreach ($destinations as $destination)
<div class="col-md-3" >
  <a href="{{URL::to('/') .'/tourist-attraction-Detail/'.$destination->destination_id."/".preg_replace('/\s+/', '-',$destination->title)}}">
  <p style="position: absolute;top: 40%;font-size: 30px;color: #fff;text-align: center;margin: 0 auto;width: 90%;z-index:111;">{{$destination->title}}</p></a>
  <div class="thumb">
      <header class="thumb-header">
          <a class="" href="{{URL::to('/') .'/tourist-attraction-Detail/'.$destination->destination_id."/".preg_replace('/\s+/', '-',$destination->title)}}" itemprop="mainEntityOfPage">
              @if (file_exists('img/thumbnail/'.$destination->image))
                <img src="{{asset('img/thumbnail/'.$destination->image)}}" class="detinationImage" alt="{{$destination->title}}" title="{{$destination->title}}" itemprop="image" height="173">
              @else
                <img src="{{asset('img/'.$destination->image)}}" class="detinationImage" alt="{{$destination->title}}" title="{{$destination->title}}" itemprop="image" height="173">
              @endif
              <img class="overlay" src="{{asset('img/overlay.png')}}">
          </a>
      </header>
  </div>
</div>
@endforeach

	</div>
</div>


@endsection
