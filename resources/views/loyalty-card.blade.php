@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')


  <div class="full-r">
    <img src="img/Loylaty-card.jpg" alt="Loyalty Card" title="Loyalty Card">
  </div>
  <div class="container" >
    <br>
    <h4>1.1	Loyalty Card Policy.</h4>
    <ul>
      <li>Get 2000 cashback on your second tour with us.</li>
      <li>You can transfer your credit to your friends and family.</li>
      <li>Lucky Draw for all loyal customers to get 1 day full cash back.</li>
    </ul>
    <h5>Note: You can only use at max of 2000 cashback back for every Trip/booking in which you have not used your previous credit.</h5>
    <ul>
      <li>You can use your cashback card within a period of 6 months.</li>
      <li>Company reserves the right to refuse the issuance of balance rewards.</li>
      <li>Only members can participate in competitions or lucky draws immediate family and friends may be referred.</li>
      <li>The card all the time remains company property and must be used only by you only for the purpose contemplated by these terms and conditions.</li>
      <li>Card is not transferable, but credit can be used by any friends and family.</li>
      <li>Each promotion and competition will be subjected to individual terms and conditions which would be specified on the website.</li>
    </ul>
    <h4>1.2	Cash Redeem policy</h4>
    <ul>
      <li>Max 500 credit can be used at spending of 10,000</li>
      <li>Max 1000 credit can be used at spending of 20,000</li>
      <li>Max 2000 credit can be used at spending of 30,000</li>
    </ul>
  </div>


@endsection
