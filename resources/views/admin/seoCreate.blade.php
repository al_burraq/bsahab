@extends('layouts.admin')
@section('body')
  <link rel="stylesheet" type="text/css" href="{{asset('css/tagsinput.css')}}">
  <script src="{{asset('js/tagsinput.js')}}"></script>


<style media="screen">
.table {
  table-layout: fixed;
}
.seen{
  color: black;
}
.unseen{
  color: red;
}
</style>
<div class="page">

  <div class="page-content"  >
    <div class=" ">
      <form  action="{{route('seoStore')}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">


      <div class="col-md-12 card card-outline-primary">
        <div class="row">
          <div class="col-md-12">
            <h3>Seo Create</h3>
          </div>
          <div class="col-md-5">
                 <div class="form-group">
                  <label for="title">Title</label>
                <div class="input-group input-group-icon">

                  <input tabindex="1" type="text" class="form-control" id="title" name="title" value="" placeholder="Title">
                </div>
              </div>
                 <div class="form-group">
                  <label for="url">Url</label>
                <div class="input-group input-group-icon">

                  <input tabindex="2" type="text" class="form-control" id="url" name="url" value="" placeholder="url">
                </div>
              </div>

         </div>
         <div class="col-md-5">
           <div class="form-group">
            <h4>Description</h4>
              <textarea class="form-control" tabindex="11" id="textareaDefault" rows="4" name="description"></textarea>
            </div>







        </div>


        </div>

        <div class="row">
          <div class="form-group" style="    margin-left: 16px;">
           <label for="tags">keywords</label>
         <div class="input-group input-group-icon">

           <input tabindex="2" type="text" class="form-control" id="tags" name="tags" value="" placeholder="keywords">
         </div>
       </div>
        </div>

      <div class="row">
        <div class="col-md-4" style="margin-bottom:20px;">
          <button type="submit" class="btn btn-primary" name="submit">Submit</button>

        </div>
      </div>



      </div>
    </form>

    </div>
  </div>

</div>
<style media="screen">
  #tags_tagsinput{
    width: 477px!important;
  }
</style>
<script type="text/javascript">
$('#tags').tagsInput();

</script>
@endsection
