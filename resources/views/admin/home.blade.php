@extends('layouts.admin')
@section('body')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js" charset="utf-8"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-chart.js/1.1.1/angular-chart.min.js" charset="utf-8"></script>
<script src="{{asset("global/vendor/chartist/chartist.min.js")}}"></script>
<script src="{{asset("global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js")}}"></script>

@verbatim
  <div class="page" ng-controller="myController" ng-app="myApp">
    <div class="page-content container-fluid">
      <div ng-controller="LineCtrl" class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-md-4" style="margin-left:0px;padding-left:0px;">
          <!-- Widget Linearea One-->
          <div class="card card-shadow" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-account grey-600 font-size-24 vertical-align-bottom mr-5"></i>Message Request
                </div>
                <span class="float-right grey-700 font-size-30">{{messageRequests}}</span>
              </div>

              <div class="ct-chart h-50"></div>
            </div>
          </div>
          <!-- End Widget Linearea One -->
        </div>
        <div class="col-md-4">
          <!-- Widget Linearea Two -->
          <div class="card card-shadow" id="widgetLineareaTwo">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-flash grey-600 font-size-24 vertical-align-bottom mr-5"></i>Pick And Drop
                </div>
                <span class="float-right grey-700 font-size-30">{{pickAndDrop}}</span>
              </div>
              <div class="ct-chart h-50"></div>
            </div>
          </div>
          <!-- End Widget Linearea Two -->
        </div>
        <div class="col-md-4" style="margin-right:0px;padding-right:0px;">
         <!-- Widget Linearea Three -->
         <div class="card card-shadow" id="widgetLineareaThree">
           <div class="card-block p-20 pt-10">
             <div class="clearfix">
               <div class="grey-800 float-left py-10">
                 <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>Vehicle Booking
               </div>
               <span class="float-right grey-700 font-size-30">{{vehicleBookingCount}}</span>
             </div>
             <div class="ct-chart h-50"></div>
           </div>
         </div>
         <!-- End Widget Linearea Three -->
       </div>


        <div class="col-md-8  offset-md-2" style="background-color:white;">
          <div class="col-md-12 text-center">
            <h2>Vehicle Booking</h2>
          </div>
          <canvas id="line" class="chart chart-line" chart-data="data"
              chart-labels="labels" chart-series="series" chart-options="options"
              chart-dataset-override="datasetOverride" chart-click="onClick">
          </canvas>
        </div>
      </div>
    </div>
  </div>

@endverbatim
<script>
var app = angular.module('myApp', ["chart.js"]);
app.controller('myController', function($scope,$http) {
  $scope.getReveresed = function(dataz){
    var data = [];
    for (var key in dataz) {
      var value = dataz[key];
      data.push(value);
    }
    var newData = [];
    for (var i = 1; i < 13; i++) {
      newData.push(data.pop());
    }
    return newData;
  }
  $scope.messageRequests = 0;
  $scope.pickAndDrop = 0;
  $scope.vehicleBookingCount = 0;

  $scope.initlizeAllDetails = function(){
  $http({
      method : "POST",
      url : "{{route('getInitilizationData')}}"
  }).then(function mySuccess(response) {
    $scope.messageRequests = response.data.messageCount;
    $scope.pickAndDrop = response.data.pickAndDrop;
    $scope.vehicleBookingCount = response.data.vehicleBookingCount;
    window.vehicleBookingData = response.data.vehicleBookingData;
    window.createFirstChart($scope.getReveresed(response.data.messageRequestsData));
    window.createSecondChart($scope.getReveresed(response.data.pickAndDropData));
    window.createThirdChart($scope.getReveresed(response.data.vehicleBookingData));

  });
  }
$scope.initlizeAllDetails();


});
</script>


<script type="text/javascript">

angular.module("myApp").controller("LineCtrl", function ($scope,$http) {
  $scope.labels = ["January", "February", "March", "April", "May", "June", "July","August","October","November","December"];
  $scope.series = ['Vehicle Booking'];
  $scope.data = [
  ];
  $scope.getReveresed = function(dataz){
    var data = [];
    for (var key in dataz) {
      var value = dataz[key];
      data.push(value);
    }
    var newData = [];
    for (var i = 1; i < 13; i++) {
      newData.push(data.pop());
    }
    return newData;
  },
  $http({
      method : "POST",
      url : "{{route('getInitilizationData')}}"
  }).then(function mySuccess(response) {
    window.vehicleBookingData = response.data.vehicleBookingData;
    $scope.MyData =   response.data.vehicleBookingData;

var monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

function zeroFill( number, width )
{
  width -= number.toString().length;
  if ( width > 0 )
  {
    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
  }
  return number + ""; // always return a string
}

var d = new Date();
var labelsArray = Array();
var values      = Array();

for (var i = 0; i < 12; i++) {
  var month = (d.getMonth()+i + 1) % 12;
  labelsArray.push(monthNames[month]);

  var monthPad = month;
  monthPad = monthPad + 13 % 12;
  monthPad = zeroFill(monthPad,2);
  if (monthPad == 0) {
    monthPad = 12
  }
    values.push($scope.MyData[monthPad]);
  }
  $scope.labels = labelsArray;

  $scope.data = [
    values
  ];

});


  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
  $scope.datasetOverride = [{ yAxisID: 'y-axis-1'}];
  $scope.options = {
    scales: {
      yAxes: [
        {
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left',

        }
      ]
    }
  };
});

</script>
<script type="text/javascript">
  window.createFirstChart = function(data){
      window.chartOne = new Chartist.Line('#widgetLineareaOne .ct-chart', {
        labels: ['1', '2', '3', '4', '5', '6', '7', '8'],
        series: [
          data
        ]
      }, {
        low: 0,
        showArea: true,
        showPoint: false,
        showLine: false,
        fullWidth: true,
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        },
        axisX: {
          showLabel: false,
          showGrid: false,
          offset: 0
        },
        axisY: {
          showLabel: false,
          showGrid: false,
          offset: 0
        }
      });

  }
  window.createSecondChart = function(data){
      new Chartist.Line('#widgetLineareaTwo .ct-chart', {
        labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        series: [
          data
        ]
      }, {
        low: 0,
        showArea: true,
        showPoint: false,
        showLine: false,
        fullWidth: true,
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        },
        axisX: {
          showLabel: false,
          showGrid: false,
          offset: 0
        },
        axisY: {
          showLabel: false,
          showGrid: false,
          offset: 0
        }
      });
    window.createThirdChart = function(data){
      new Chartist.Line('#widgetLineareaThree .ct-chart', {
        labels: ['1', '2', '3', '4', '5', '6', '7', '8'],
        series: [
          data
        ]
      }, {
        low: 0,
        showArea: true,
        showPoint: false,
        showLine: false,
        fullWidth: true,
        chartPadding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        },
        axisX: {
          showLabel: false,
          showGrid: false,
          offset: 0
        },
        axisY: {
          showLabel: false,
          showGrid: false,
          offset: 0
        }
      });

    }
  }
</script>
<script type="text/javascript">
// you can omit the 'window' keyword
if (window.webkitNotifications) {
  console.log("Notifications are supported!");
}
else {
  console.log("Notifications are not supported for this Browser/OS version yet.");
}
</script>
<style media="screen">
  #line {
    /*height: 400px !important;*/
  }
</style>
@endsection
