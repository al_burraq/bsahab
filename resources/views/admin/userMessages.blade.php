@extends('layouts.admin')
@section('body')
<style media="screen">
.table {
  table-layout: fixed;
}
.seen{
  color: black;
}
.unseen{
  color: red;
}
</style>
<div class="page">
  @verbatim
  <div class="page-content" ng-app="myApp" ng-controller="myCtrl">
    <div class="row">
      <div class="col-md-12 card card-outline-primary">

        <table class="table panel panel-default table-hover" style="margin-top:10px;">
          <thead>
            <tr>
              <th >Name</th>
              <th >Email</th>
              <th >Message</th>
              <th>CreatedAt</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

              <tr ng-repeat="message in messages">
                <td>{{message.name}}</td>
                <td>{{message.email}}</td>
                <td>{{message.message}}</td>
                <td>{{message.createdAt}}</td>
                <td>
                  <a href="javascript:void(0)" ng-click="changeState(message)" ng-class="message.isReaded"><i style="font-size: 20px;"  class="fa fa-eye" aria-hidden="true"></i></a>
                  <a href="javascript:void(0)" ng-click="deleteMesssage(message)"  style="margin-left: 13px;"><i style="font-size: 20px;color:black;"  class="fa fa-trash-o" aria-hidden="true"></i></a>
                </td>
              </tr>

      </tbody>
    </table>
      </div>

    </div>
  </div>
  @endverbatim
</div>
<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope,$http) {
    $scope.messages = [];

    $http({
        method : "GET",
        url : "{{route('getUserMessageJson')}}"
    }).then(function mySuccess(response) {
        $scope.messages = response.data;
    });
    $scope.deleteMesssage = function(message){
      id = message.id;
      objIndex = $scope.messages.findIndex(function(obj){
        return obj.id == id
      });
      $scope.messages.splice(objIndex, 1);
      $http({
          method : "post",
          url : "{{route('deleteUserMessage')}}",
          data : {
            id: id
          }
      });
    }
    $scope.changeState = function(message){
      id = message.id;
      objIndex = $scope.messages.findIndex(function(obj){
        return obj.id == id
      });
      var isReaded = $scope.messages[objIndex].isReaded;
      if (isReaded == 'seen') {
        $scope.messages[objIndex].isReaded = 'unseen';
      }
      if (isReaded == 'unseen') {
        $scope.messages[objIndex].isReaded = 'seen';
      }
      $http({
          method : "post",
          url : "{{route('setUserMessageSeen')}}",
          data : {
            id: id,
            isReaded: $scope.messages[objIndex].isReaded
          }
      });
    };

});
</script>

@endsection
