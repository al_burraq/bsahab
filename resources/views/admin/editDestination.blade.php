@extends('layouts.admin')
@section('css')
<link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
@endsection
@section('body')
<div class="page">
  <form method="post" action="{{route('updateDestination',$destination->destination_id)}}" enctype="multipart/form-data">
    {{ csrf_field()}}
    @if (Session::has('status'))
      <br>
    <div class="card">
      <div class="card-block card-primary" style="color: white;font-size: 23px;text-align: center;">
        <p class="card-text">
          {{Session::get('status')}}
        </p>
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col-md-6">

      <br>
      <div class="panel-body panel container-fluid">
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="Title">Title</label>
            <input type="text" class="form-control" value="{{$destination->title}}" id="Title" name="title" placeholder="Title">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="longitude">Longitude</label>
            <input type="text" class="form-control" value="{{$destination->longitude}}" id="longitude" name="longitude" placeholder="Longitude">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="Latitude">Latitude</label>
            <input type="text" class="form-control" id="Latitude" value="{{$destination->latitude}}" name="latitude" placeholder="Latitude">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="Parent">Parent</label>
            <input type="text" class="form-control" id="Parent" value="{{$destination->parent}}" name="parent" placeholder="Parent">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="emergencyno">Emergency Number</label>
            <input type="text" class="form-control" id="emergencyno" value="{{$destination->emergency_number}}" name="emergencyno" placeholder="Emergency Number">
          </div>

          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="videoLink">Video Link</label>
            <input type="text" class="form-control" id="videoLink" value="{{$destination->videoLink}}" name="videoLink" placeholder="Video Link">
          </div>

          <div class="row">
            <label class="pt-3 col-md-12" for="manual_transmission">Gallery Images</label>
            @foreach ($destinationGallary as $obj)

            <div id="gallery{{$obj->id}}" class="col-md-6" style="padding-left:0px;margin-bottom:10px;">
              <a onclick="deleteGalleryImage({{$obj->id}})" style="font-size:10px;" class="pull-right panel-action icon md-close" data-toggle="panel-close" aria-hidden="true"></a>
              <img class="img-fluid img-thumbnail" src="{{asset("img/$destination->destination_id/$obj->image")}}" alt="">
            </div>
            @endforeach
        </div>
      </div>

      <button type="submit" id="submit" class="btn btn-primary">Submit</button>
      <a class="btn btn-primary" href="{{route('adminDestination')}}">Back</a>
    </div>
  <div class="col-md-6">
      <br>
      <div class="panel-body panel container-fluid" style="padding-bottom:46px;">
          <div class="col-md-6" style="padding-left:0px;margin-bottom:10px;">
            <img class="img-fluid" src="{{asset("img/$destination->image")}}" alt="">
          </div>
          <div class="form-group form-material form-material-file" data-plugin="formMaterial">
            <label class="form-control-label" for="image">Image</label>
            <input type="text" class="form-control" placeholder="Browse.." readonly="">
            <input type="file" id="image" name="image" accept="image/*">
          </div>

          <div class="form-group form-material form-material-file" data-plugin="formMaterial">
            <label class="form-control-label" for="galleryImages">Gallery Images</label>
            <input type="text" class="form-control" placeholder="Browse.." readonly="">
            <input type="file" id="galleryImages" name="galleryImages[]" multiple accept="image/*">
          </div>



          <div id="toolbar-container">
          <span class="ql-formats">
          <select class="ql-font"></select>
          <select class="ql-size"></select>
          </span>
          <span class="ql-formats">
          <button class="ql-bold"></button>
          <button class="ql-italic"></button>
          <button class="ql-underline"></button>
          <button class="ql-strike"></button>
          </span>
          <span class="ql-formats">
          <select class="ql-color"></select>
          <select class="ql-background"></select>
          </span>
          <span class="ql-formats">
          <button class="ql-script" value="sub"></button>
          <button class="ql-script" value="super"></button>
          </span>
          <span class="ql-formats">
          <button class="ql-header" value="1"></button>
          <button class="ql-header" value="2"></button>
          <button class="ql-blockquote"></button>
          <button class="ql-code-block"></button>
          </span>
          <span class="ql-formats">
          <button class="ql-list" value="ordered"></button>
          <button class="ql-list" value="bullet"></button>
          <button class="ql-indent" value="-1"></button>
          <button class="ql-indent" value="+1"></button>
          </span>
          <span class="ql-formats">
          <button class="ql-direction" value="rtl"></button>
          <select class="ql-align"></select>
          </span>
          <span class="ql-formats">
          <button class="ql-link"></button>
          <button class="ql-image"></button>
          <button class="ql-video"></button>
          <button class="ql-formula"></button>
          </span>
          <span class="ql-formats">
          <button class="ql-clean"></button>
          </span>
          </div>
          <input name="description" type="text" value="" id="descriptionId" style="display:none;">

          <div id="description" style="height:200px;"></div>

          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="select">Province</label>
            <select class="form-control" name="province" id="select">
              <option @if($destination->province == 1) selected @endif value="1">Punjab</option>
              <option @if($destination->province == 2) selected @endif value="2">Sindh</option>
              <option @if($destination->province == 3) selected @endif value="3">Khyber PakhtunKhwa</option>
              <option @if($destination->province == 4) selected @endif value="4">Balochistan</option>
              <option @if($destination->province == 5) selected @endif value="5">Gilgit Baltistan</option>
              <option @if($destination->province == 6) selected @endif value="6">Azad Kashmir</option>
            </select>
          </div>

      </div>

    </div>


  </div>



</form>
</div>

<script type="text/javascript">
function deleteGalleryImage(id){
  if (confirm('Are you sure you want to delete this Gallery Image ?')) {
    $("#gallery"+id).remove();
    $.ajax({
            method: "POST",
            url: "{{route('deleteDestinationGalleryImage')}}",
            data: {
              _token: "{{csrf_token()}}",
              id: id
            }
        });
  }
}
</script>

@endsection
@section('js')
  <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
  <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
@endsection



@section('script')
  <script type="text/javascript">

    var editor = new Quill('#description', {
                  debug: 'info',
                  modules: {
                    toolbar: '#toolbar-container'
                  },
                  placeholder: 'Compose an epic...',
                  theme: 'snow'
                });
    window.editor = editor;
    
  // description
  $("#submit").click(function(event) {
    $("#descriptionId").val(window.editor.root.innerHTML);
  });
  $(document).ready(function() {
    window.editor.root.innerHTML =  "{!! $destination->description!!}";
  });
  </script>
@endsection
