@extends('layouts.admin')
@section('body')
<div class="page">
  <div class="page-content">
    @foreach ($messages as $obj)
      <div class="col-md-4">
          <div class="panel panel-info panel-line">
            <div class="panel-heading">
              <h3 class="panel-title">{{$obj->name}} - {{$obj->email}}</h3>
            </div>
            <div class="panel-body">
              {{$obj->message}}
            </div>
          </div>
        </div>
    @endforeach
  </div>
</div>
@endsection
