@extends('layouts.admin')
@section('body')
<style media="screen">
.table {
  table-layout: fixed;
}
.seen{
  color: black;
}
.unseen{
  color: red;
}
</style>
<div class="page">
  @verbatim
  <div class="page-content" ng-app="myApp" ng-controller="myCtrl">
    <div class="row">
      <div class="col-md-12 card card-outline-primary">

        <table class="table panel panel-default table-hover" style="margin-top:10px;">
          <thead>
            <tr>

              <th >Email</th>

              <th>CreatedAt</th>

            </tr>
          </thead>
          <tbody>

              <tr ng-repeat="subscriber in subscribLetterList">

                <td>{{subscriber.email}}</td>

                <td>{{subscriber.createdAt}}</td>

              </tr>

      </tbody>
    </table>
      </div>

    </div>
  </div>
  @endverbatim
</div>
<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope,$http) {
    $scope.subscribLetterList = [];

    $http({
        method : "GET",
        url : "{{route('subscriberData')}}"
    }).then(function mySuccess(response) {
        // console.log(response.data);
        $scope.subscribLetterList = response.data;
    });
    $scope.deleteMesssage = function(item){
      id = item.id;
      objIndex = $scope.subscribLetterList.findIndex(function(obj){
        return obj.id == id
      });
      $scope.subscribLetterList.splice(objIndex, 1);
      $http({
          method : "post",
          url : "{{route('deleteSubsriber')}}",
          data : {
            id: id
          }
      });
    }
    $scope.changeState = function(message){
      id = message.id;
      objIndex = $scope.messages.findIndex(function(obj){
        return obj.id == id
      });
      var isReaded = $scope.messages[objIndex].isReaded;
      if (isReaded == 'seen') {
        $scope.messages[objIndex].isReaded = 'unseen';
      }
      if (isReaded == 'unseen') {
        $scope.messages[objIndex].isReaded = 'seen';
      }
      $http({
          method : "post",
          url : "{{route('setUserMessageSeen')}}",
          data : {
            id: id,
            isReaded: $scope.messages[objIndex].isReaded
          }
      });
    };

});
</script>

@endsection
