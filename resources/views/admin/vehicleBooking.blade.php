@extends('layouts.admin')
@section('body')
<style media="screen">
.table {
  table-layout: fixed;
}
.seen{
  color: black;
}
.unseen{
  color: red;
}
</style>
<div class="page">
  @verbatim
  <div class="page-content" ng-app="myApp" ng-controller="myCtrl">
    <div class="row">
      <div class="col-md-12 card card-outline-primary">

        <table class="table panel panel-default table-hover" style="margin-top:10px;">
          <thead>
            <tr>
              <th >Vehicle</th>
              <th >PickUp Location</th>
              <th >Drop Location</th>
              <th>Phone</th>
              <th>Pick Date</th>
              <th>Drop Date</th>
              <th>Received Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

              <tr ng-repeat="booking in bookings">
                <td>{{booking.vehicle}}</td>
                <td>{{booking.pickUpLocation}}</td>
                <td>{{booking.dropLocation}}</td>

                <td>{{booking.phone}}</td>
                <td>{{booking.pickDate}}</td>
                <td>{{booking.dropDate}}</td>
                <td>{{booking.createdAt}}</td>
                <td>
                  <a href="javascript:void(0)" ng-click="changeState(booking)" ng-class="booking.isReaded"><i style="font-size: 20px;"  class="fa fa-eye" aria-hidden="true"></i></a>
                  <a href="javascript:void(0)" ng-click="deleteBooking(booking)"  style="margin-left: 13px;"><i style="font-size: 20px;color:black;"  class="fa fa-trash-o" aria-hidden="true"></i></a>
                </td>
              </tr>

      </tbody>
    </table>
      </div>

    </div>
  </div>
  @endverbatim
</div>
<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope,$http) {
    $scope.bookings = [];

    $http({
        method : "POST",
        url : "{{route('getVehicleBooking')}}"
    }).then(function mySuccess(response) {
        $scope.bookings = response.data;
    });
    $scope.deleteBooking = function(booking){
      if (!confirm('are you sure you want to remove this booking ?')) {
        return 0;
      }
      id = booking.id;
      objIndex = $scope.bookings.findIndex(function(obj){
        return obj.id == id
      });
      $scope.bookings.splice(objIndex, 1);
      $http({
          method : "post",
          url : "{{route('deleteBooking')}}",
          data : {
            id: id
          }
      });
    }
    $scope.changeState = function(booking){
      id = booking.id;
      objIndex = $scope.bookings.findIndex(function(obj){
        return obj.id == id
      });
      var isReaded = $scope.bookings[objIndex].isReaded;
      if (isReaded == 'seen') {
        $scope.bookings[objIndex].isReaded = 'unseen';
      }
      if (isReaded == 'unseen') {
        $scope.bookings[objIndex].isReaded = 'seen';
      }
      $http({
          method : "post",
          url : "{{route('setBookingSeen')}}",
          data : {
            id: id,
            isReaded: $scope.bookings[objIndex].isReaded
          }
      });
    };

});
</script>

@endsection
