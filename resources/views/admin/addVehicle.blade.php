@extends('layouts.admin')
@section('body')
<div class="page">

  <form method="post" action="{{route('addVehicle')}}" enctype="multipart/form-data" >
    {{ csrf_field()}}
    @if (Session::has('status'))
      <br>
    <div class="card">
      <div class="card-block card-primary" style="color: white;font-size: 23px;text-align: center;">
        <p class="card-text">
          {{Session::get('status')}}
        </p>
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col-md-6"  style="margin-bottom:50px;">
      <br>
      <div class="panel-body panel container-fluid">
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="vehicleMaker">Vehicle Maker</label>
            <input type="text" class="form-control" id="vehicleMaker" name="vehicleMaker" placeholder="Vehicle Maker">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="model">Model</label>
            <input type="text" class="form-control" id="model" name="model" placeholder="Model">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="passenger">Passenger</label>
            <input type="text" class="form-control" id="passenger" name="passenger" placeholder="Passenger">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="baggageQuantity">Baggage Quantity</label>
            <input type="text" class="form-control" id="baggageQuantity" name="baggageQuantity" placeholder="Baggage Quantity">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="doors">Doors</label>
            <input type="text" class="form-control" id="doors" name="doors" placeholder="Doors">
          </div>
          <div class="form-group form-material form-material-file" data-plugin="formMaterial">
            <label class="form-control-label" for="image">Feature Image</label>
            <input type="text" class="form-control" placeholder="Browse.." readonly="">
            <input type="file" id="image" name="image" accept="image/*">
          </div>
          <div class="form-group form-material form-material-file" data-plugin="formMaterial">
            <label class="form-control-label" for="galleryImages">Gallery Images</label>
            <input type="text" class="form-control" placeholder="Browse.." readonly="">
            <input type="file" id="galleryImages" name="galleryImages[]" accept="image/*" multiple>
          </div>

      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
      <a class="btn btn-primary" href="{{route('adminDestination')}}">Back</a>
    </div>
  <div class="col-md-6">
      <br>

      <div class="panel-body panel container-fluid" style="padding-bottom: 4px;">
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="perDayRate">Per Day Rate</label>
            <input type="text" class="form-control" id="perDayRate" name="perDayRate" placeholder="Per Day Rate">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="mileAge">Mile Age</label>
            <input type="text" class="form-control" id="mileAge" name="mileAge" placeholder="Mile Age">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="phoneNumber">Phone Number</label>
            <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="Phone Number">
          </div>

            <div class="form-group form-material" data-plugin="formMaterial">
              <label class="form-control-label" for="Engine">Engine</label>
              <select class="form-control" name="Engine" id="Engine">
                <option value="petrol">Petrol</option>
                <option value="diesel">Diesel</option>
                <option value="hybrid vehicle">Hybrid Vehicle</option>
              </select>
            </div>

          <div class="row">

            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="tilt_steering_wheel">Tilt Steering Wheel</label>
                  <div class="mr-20">
                    <input type="checkbox" id="tilt_steering_wheel" name="tilt_steering_wheel" data-plugin="switchery" checked />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="lcd">LCD</label>
                  <div class="mr-20">
                    <input type="checkbox" id="lcd" name="lcd" data-plugin="switchery" checked />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="power_door_locks">Power Door Locks</label>
                  <div class="mr-20">
                    <input type="checkbox" id="power_door_locks" name="power_door_locks" data-plugin="switchery" checked />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="satellite_navigation">Satellite Navigation</label>
                  <div class="mr-20">
                    <input type="checkbox" id="satellite_navigation" name="satellite_navigation" data-plugin="switchery" checked />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="air_conditioning">Air Conditioning</label>
                  <div class="mr-20">
                    <input type="checkbox" id="air_conditioning" name="air_conditioning" data-plugin="switchery" checked />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="climate_controll">Climate Controll</label>
                  <div class="mr-20">
                    <input type="checkbox" id="climate_controll" name="climate_controll" data-plugin="switchery" checked />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="manual_transmission">Manual Transmission</label>
                  <div class="mr-20">
                    <input type="checkbox" id="manual_transmission" name="manual_transmission" data-plugin="switchery" checked />
                  </div>
              </div>
            </div>


        </div>






      </div>
    </div>
  </div>
</form>
</div>
<script type="text/javascript">

</script>

@endsection
