@extends('layouts.admin')
@section('body')
<div class="page">
  @if (Session::has('status'))
    <br>
  <div class="card">
    <div class="card-block card-primary" style="color: white;font-size: 23px;text-align: center;">
      <p class="card-text">
        {{Session::get('status')}}
      </p>
    </div>
  </div>
@endif
  <div class="row">
  <div class="col-md-12" style="margin-top:10px;">
    <div class="pull-right">
      <a class="btn btn-primary waves-effect waves-classic" href="{{route('createVehicle')}}">Add new vehicle</a>
    </div>
  </div>
</div>
  <br>
  <div class="row">
    <div class="col-md-12 card card-outline-primary">

      <table class="table panel panel-default table-hover" style="margin-top:10px;">
        <thead>
          <tr>
            <th>Name</th>
            <th>Model</th>
            <th>Passenger</th>
            <th>Baggage Quantity</th>
            <th>Per Day Rate</th>
            <th>Mile Age</th>
            <th>Phone Number</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($vehicles as $obj)
            <tr id="{{$obj->id}}">
              <td>{{$obj->vehicle_make}} - {{$obj->name}}</td>
              <td>{{$obj->model}}</td>
              <td>{{$obj->passenger}}</td>
              <td>{{$obj->baggage_quantity}}</td>
              <td>{{$obj->per_day_rate}}</td>
              <td>{{$obj->mile_age}}</td>
              <td>{{$obj->phone_number}}</td>
              <td>

                  <a style="font-size:23px;text-decoration: none;color:#36459b;padding-right:10px;"  href="{{route('editVehicle',$obj->id)}}" class="panel-action fa fa-pencil-square-o" aria-hidden="true"></a>

                  <a style="font-size:25px;text-decoration: none;color:#36459b;    cursor: pointer;" onclick="deleteVehicle({{$obj->id}})"  class="panel-action icon md-close" data-toggle="panel-close" aria-hidden="true"></a>

              </td>
            </tr>
          @endforeach
    </tbody>
  </table>
    </div>

  </div>
</div>
<script type="text/javascript">
  function deleteVehicle(id){
    if (confirm('Are you sure you want to delete this Vehicle ?')) {
      $("#"+id).remove();
      $.ajax({
              method: "POST",
              url: "{{route('deleteVehicle')}}",
              data: {
                _token: "{{csrf_token()}}",
                id: id
              }
          });
    }
  }
</script>
@endsection
