@extends('layouts.admin')
@section('body')
<style media="screen">


.panel-body {
  padding: 30px 8px;
}
</style>
<div class="page">
  @if (Session::has('status'))
    <br>
  <div class="card">
    <div class="card-block card-primary" style="color: white;font-size: 23px;text-align: center;">
      <p class="card-text">
        {{Session::get('status')}}
      </p>
    </div>
  </div>
@endif
<div class="row">
  <div class="col-md-12" style="margin-top:10px;">
    <div class="pull-right">
      <a class="btn btn-primary" href="{{route('createDestination')}}">Add new destination</a>
    </div>
  </div>
</div>
<div class="row">
    @foreach ($destinations as $obj)
      <div class="col-md-6" id="{{$obj->destination_id}}">
        <div class="panel-body">
          <div>
            <div class="panel panel-info panel-line" style=" max-height: 150px;overflow: hidden;margin-top:5px;">
              <div class="col-md-6 pull-left panel panel-heading panel-toolbar" style="padding:0px;padding-right:15px;padding-left5px;">
                <img src="@if(file_exists('img/thumbnail/'.$obj->image)){{asset('img/thumbnail/'.$obj->image)}}@else{{asset('img/'.$obj->image)}}@endif" class="img-fluid" alt="">
              </div>
                <div class="panel-heading">
                  <h3 class="panel-title offset-md-6" style="padding: 10px 10px;">{{$obj->title}}
                    <div class="pull-right">
                      <div class="panel-actions">
                        <a style="font-size:24px;" href="{{route('editDestination',$obj->destination_id)}}" class="panel-action fa fa-pencil-square-o" aria-hidden="true"></a>
                        <a onclick="deleteDestination({{$obj->destination_id}})" style="font-size:24px;" class="panel-action icon md-close" data-toggle="panel-close" aria-hidden="true"></a>
                      </div>
                    </div>
                  </h3>
                </div>
                <div class="panel-body offset-md-6" style="font-size: 12px;">
                  {{substr($obj->description,0,260)}}
                </div>
            </div>
          </div>
        </div>
      </div>
    @endforeach
</div>

</div>
<script type="text/javascript">
  function deleteDestination(id){
    if (confirm('Are you sure you want to delete this destination ?')) {
      $("#"+id).remove();
      $.ajax({
              method: "POST",
              url: "{{route('deleteDestination')}}",
              data: {
                _token: "{{csrf_token()}}",
                id: id
              }
          });
    }
  }
</script>
@endsection
