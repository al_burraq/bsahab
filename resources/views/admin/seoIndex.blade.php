@extends('layouts.admin')
@section('body')
<style media="screen">
.table {
  table-layout: fixed;
}
.seen{
  color: black;
}
.unseen{
  color: red;
}
</style>
<div class="page">
  @verbatim
  <div class="page-content" ng-app="myApp" ng-controller="myCtrl">

    <div class="row">

      <div class="col-md-12 card card-outline-primary">
        <div class="" style="padding-top:10px;">
          <a class="btn btn-primary pull-right" href="/admin/seoCreate">Create New</a>
        </div>

        <table class="table panel panel-default table-hover" style="margin-top:10px;">
          <thead>
            <tr>

              <th >Url</th>

              <th>Title</th>
              <th>Keywords</th>
              <th>Description</th>
              <th style="text-align:center;">Action</th>
            </tr>
          </thead>
          <tbody>

              <tr ng-repeat="item in seo">
                <td  >{{item.url}}</td>

                <td>{{item.title}}</td>
                <td>{{item.keywords}}</td>
                <td>{{item.desciption}}</td>
                <td style="text-align:center;font-size:20px;;">
                  <a href="/admin/seoEdit/{{item.id}}">
                    <i class="fa fa-pencil" aria-hidden="true"></i>

                  </a>
                </td>


              </tr>

      </tbody>
    </table>
      </div>

    </div>
  </div>
  @endverbatim
</div>
<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope,$http) {
    $scope.seo = [];

    $http({
        method : "GET",
        url : "{{route('seoCollection')}}"
    }).then(function mySuccess(response) {
      console.log(response.data);
        // console.log(response.data);
        $scope.seo = response.data;
    });
    $scope.deleteMesssage = function(item){
      id = item.id;
      objIndex = $scope.seo.findIndex(function(obj){
        return obj.id == id
      });
      $scope.seo.splice(objIndex, 1);
      $http({
          method : "post",
          url : "{{route('deleteSubsriber')}}",
          data : {
            id: id
          }
      });
    }
    $scope.changeState = function(message){
      id = message.id;
      objIndex = $scope.messages.findIndex(function(obj){
        return obj.id == id
      });
      var isReaded = $scope.messages[objIndex].isReaded;
      if (isReaded == 'seen') {
        $scope.messages[objIndex].isReaded = 'unseen';
      }
      if (isReaded == 'unseen') {
        $scope.messages[objIndex].isReaded = 'seen';
      }
      $http({
          method : "post",
          url : "{{route('setUserMessageSeen')}}",
          data : {
            id: id,
            isReaded: $scope.messages[objIndex].isReaded
          }
      });
    };

});
</script>

@endsection
