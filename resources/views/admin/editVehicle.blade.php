@extends('layouts.admin')
@section('body')
<div class="page">

  <form method="post" action="{{route('updateVehicle',$vehicle->id)}}" enctype="multipart/form-data">
    {{ csrf_field()}}
    @if (Session::has('status'))
      <br>
    <div class="card">
      <div class="card-block card-primary" style="color: white;font-size: 23px;text-align: center;">
        <p class="card-text">
          {{Session::get('status')}}
        </p>
      </div>
    </div>
  @endif
  <div class="row">
    <div class="col-md-6" style="margin-bottom:50px;">
      <br>
      <div class="panel-body panel container-fluid">
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="name">Name</label>
            <input type="text" class="form-control" value="{{$vehicle->name}}" id="name" name="name" placeholder="Name">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="vehicleMaker">Vehicle Maker</label>
            <input type="text" class="form-control" id="vehicleMaker" value="{{$vehicle->vehicle_make}}" name="vehicleMaker" placeholder="Vehicle Maker">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="model">Model</label>
            <input type="text" class="form-control" id="model" value="{{$vehicle->model}}" name="model" placeholder="Model">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="passenger">Passenger</label>
            <input type="text" class="form-control" id="passenger" value="{{$vehicle->passenger}}" name="passenger" placeholder="Passenger">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="baggageQuantity">Baggage Quantity</label>
            <input type="text" class="form-control" id="baggageQuantity" value="{{$vehicle->baggage_quantity}}" name="baggageQuantity" placeholder="Baggage Quantity">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="doors">Doors</label>
            <input type="text" class="form-control" id="doors" value="{{$vehicle->doors}}" name="doors" placeholder="Doors">
          </div>

          <div class="col-md-6" style="padding-left:0px;margin-bottom:10px;">
            <img class="img-fluid img-thumbnail" src="{{asset("img/$vehicle->id/$vehicle->image")}}" alt="">
          </div>
          <div class="form-group form-material form-material-file" data-plugin="formMaterial">
            <label class="form-control-label" for="image">Image</label>
            <input type="text" class="form-control" placeholder="Browse.." readonly="">
            <input type="file" id="image" name="image" accept="image/*">
          </div>
          <div class="form-group form-material form-material-file" data-plugin="formMaterial">
            <label class="form-control-label" for="galleryImages">Gallery Images</label>
            <input type="text" class="form-control" placeholder="Browse.." readonly="">
            <input type="file" id="galleryImages" name="galleryImages[]" accept="image/*" multiple>
          </div>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
      <a class="btn btn-primary" href="{{route('adminDestination')}}">Back</a>
    </div>
  <div class="col-md-6">
      <br>

      <div class="panel-body panel container-fluid" style="padding-bottom: 4px;">
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="perDayRate">Per Day Rate</label>
            <input type="text" class="form-control" id="perDayRate" value="{{$vehicle->per_day_rate}}" name="perDayRate" placeholder="Per Day Rate">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="mileAge">Mile Age</label>
            <input type="text" class="form-control" id="mileAge"  value="{{$vehicle->mile_age}}" name="mileAge" placeholder="Mile Age">
          </div>
          <div class="form-group form-material" data-plugin="formMaterial">
            <label class="form-control-label" for="phoneNumber">Phone Number</label>
            <input type="text" class="form-control" id="phoneNumber" value="{{$vehicle->phone_number}}" name="phoneNumber" placeholder="Phone Number">
          </div>

            <div class="form-group form-material" data-plugin="formMaterial">
              <label class="form-control-label" for="Engine">Engine</label>
              <select class="form-control" name="Engine" id="Engine">
                <option @if($vehicle->engine == "petrol") selected @endif value="petrol">Petrol</option>
                <option @if($vehicle->engine == "diesel") selected @endif value="diesel">Diesel</option>
                <option @if($vehicle->engine == 'hybrid vehicle') selected @endif value="hybrid vehicle">Hybrid Vehicle</option>
              </select>
            </div>

          <div class="row">

            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="tilt_steering_wheel">Tilt Steering Wheel</label>
                  <div class="mr-20">
                    <input type="checkbox" id="tilt_steering_wheel" name="tilt_steering_wheel" data-plugin="switchery" @if($vehicle->tilt_steering_wheel == "yes") checked @endif />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="lcd">LCD</label>
                  <div class="mr-20">
                    <input type="checkbox" id="lcd" name="lcd" data-plugin="switchery"  @if($vehicle->lcd == "yes") checked @endif />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="power_door_locks">Power Door Locks</label>
                  <div class="mr-20">
                    <input type="checkbox" id="power_door_locks" name="power_door_locks" data-plugin="switchery" @if($vehicle->power_door_locks == "yes") checked @endif />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="satellite_navigation">Satellite Navigation</label>
                  <div class="mr-20">
                    <input type="checkbox" id="satellite_navigation" name="satellite_navigation" data-plugin="switchery" @if($vehicle->satellite_navigation == "yes") checked @endif />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="air_conditioning">Air Conditioning</label>
                  <div class="mr-20">
                    <input type="checkbox" id="air_conditioning" name="air_conditioning" data-plugin="switchery" @if($vehicle->air_conditioning == "yes") checked @endif />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="climate_controll">Climate Controll</label>
                  <div class="mr-20">
                    <input type="checkbox" id="climate_controll" name="climate_controll" data-plugin="switchery" @if($vehicle->climate_controll == "yes") checked @endif />
                  </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group form-material" data-plugin="formMaterial">
                  <label class="pt-3" for="manual_transmission">Manual Transmission</label>
                  <div class="mr-20">
                    <input type="checkbox" id="manual_transmission" name="manual_transmission" data-plugin="switchery" @if($vehicle->manual_transmission == "yes") checked @endif />
                  </div>
              </div>
            </div>


        </div>
        <div class="row">
          <label class="pt-3 col-md-12" for="manual_transmission">Gallery Images</label>
          @foreach ($gallery as $obj)

          <div id="gallery{{$obj->id}}" class="col-md-6" style="padding-left:0px;margin-bottom:10px;">
            <a onclick="deleteGalleryImage({{$obj->id}})" style="font-size:10px;" class="pull-right panel-action icon md-close" data-toggle="panel-close" aria-hidden="true"></a>
            <img class="img-fluid img-thumbnail" src="{{asset("img/$vehicle->id/$obj->vehicleImage")}}" alt="">
          </div>
          @endforeach
      </div>
      </div>
    </div>
  </div>
</form>
</div>
<script type="text/javascript">
  function deleteGalleryImage(id){
    if (confirm('Are you sure you want to delete this Gallery Image ?')) {
      $("#gallery"+id).remove();
      $.ajax({
              method: "POST",
              url: "{{route('deleteGalleryImage')}}",
              data: {
                _token: "{{csrf_token()}}",
                id: id
              }
          });
    }
  }
</script>


@endsection
