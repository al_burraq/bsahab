@extends('layouts.admin')
@section('body')
<style media="screen">
.table {
  table-layout: fixed;
}
.seen{
  color: black;
}
.unseen{
  color: red;
}
</style>
<div class="page">
  @verbatim
  <div class="page-content" ng-app="myApp" ng-controller="myCtrl">
    <div class="row">
      <div class="col-md-12 card card-outline-primary">

        <table class="table panel panel-default table-hover" style="margin-top:10px;">
          <thead>
            <tr>
              <th >Name</th>
              <th >Phone</th>
              <th >Institute</th>
              <th>Vehicle Type</th>
              <th>Description</th>
              <th>Receive Date</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

              <tr ng-repeat="pickAndDrop in pickAndDrops">
                <td>{{pickAndDrop.name}}</td>
                <td>{{pickAndDrop.phone}}</td>
                <td>{{pickAndDrop.institute}}</td>

                <td>{{pickAndDrop.vehicleType}}</td>
                <td>{{pickAndDrop.description}}</td>
                <td>{{pickAndDrop.createdAt}}</td>
                <td>
                  <a href="javascript:void(0)" ng-click="changeState(pickAndDrop)" ng-class="pickAndDrop.isReaded"><i style="font-size: 20px;"  class="fa fa-eye" aria-hidden="true"></i></a>
                  <a href="javascript:void(0)" ng-click="deletePickAndDrop(pickAndDrop)"  style="margin-left: 13px;"><i style="font-size: 20px;color:black;"  class="fa fa-trash-o" aria-hidden="true"></i></a>
                </td>
              </tr>

      </tbody>
    </table>
      </div>

    </div>
  </div>
  @endverbatim
</div>
<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope,$http) {
    $scope.pickAndDrops = [];

    $http({
        method : "POST",
        url : "{{route('getPickDrop')}}"
    }).then(function mySuccess(response) {
        $scope.pickAndDrops = response.data;
    });
    $scope.deletePickAndDrop  = function(pickAndDrop){
      if (!confirm('are you sure you want to remove this Pick and Drop ?')) {
        return 0;
      }
      id = pickAndDrop.id;
      objIndex = $scope.pickAndDrops.findIndex(function(obj){
        return obj.id == id
      });
      $scope.pickAndDrops.splice(objIndex, 1);
      $http({
          method : "post",
          url : "{{route('deletePickAndDrop')}}",
          data : {
            id: id
          }
      });
    }
    $scope.changeState = function(pickAndDrop){
      id = pickAndDrop.id;
      objIndex = $scope.pickAndDrops.findIndex(function(obj){
        return obj.id == id
      });
      var isReaded = $scope.pickAndDrops[objIndex].isReaded;
      if (isReaded == 'seen') {
        $scope.pickAndDrops[objIndex].isReaded = 'unseen';
      }
      if (isReaded == 'unseen') {
        $scope.pickAndDrops[objIndex].isReaded = 'seen';
      }
      $http({
          method : "post",
          url : "{{route('setPickAndDrop')}}",
          data : {
            id: id,
            isReaded: $scope.pickAndDrops[objIndex].isReaded
          }
      });
    };

});
</script>

@endsection
