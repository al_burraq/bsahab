@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')

  <div class="full-r"><img src="{{asset("img/service-banner.jpg")}}"  alt="Rental Car Services - Pick and Drop Services - Bhai Sahab transport" title="Rental Car Services - Pick and Drop Services - Bhai Sahab transport"/></div>
  <div class="container" itemscope itemtype="http://schema.org/ProfessionalService">
  	<div class="row" itemprop="description">
  		<div class="col-md-12">
              <div class="col-sm-8 col-md-8 col-lg-9" style="text-align: justify;line-height:27px;padding: 0px 20px 15px;font-size:17px;font-family:'Roboto', arial, helvetica, sans-serif;">
                  <h1 style="margin-top:20px;">Lahore Rent a Car Service </h1>
                  <p>Bhai Sahab Transport is well-recognized transport company across Lahore. We deliver innovative Lahore Rent a Car service at most affordable packages that can afford by every user. Our mission is to serve citizens with our convenient travel services anywhere in Lahore. With our comfortable cars and most caring staff, you can enjoy your every tour and make it memorable. We have well-trained drivers and most efficient cars to save your precious time during travel. With our huge experience, we can meet all your travel requirements with our top quality Lahore rent a car service. So, if you are planning for long term or short term tour, stop looking anywhere else because Bhai Sahab Transport is reasonable choice for you. Let’s consult our team to hire the best car on rent.</p>
              </div>

              <div class="col-sm-4 col-md-4 col-lg-3">
              <div class="row">
                  <h5 class="" style="margin-top: 20px; border-bottom: 2px solid rgb(237, 131, 35); padding-bottom: 5px;">Contact Us for pick and drop service with in Lahore.</h5>
                  <aside class="sidebar-left">
                    <form id="service-contact">
                        <div class="form-group form-group-icon-left">
                            <input maxlength="150" class="form-control form-control-2" value="" id="name" placeholder="Name" type="text">
                            <span class="spanError form-control-2" id="spanContactName"></span>
                        </div>
                        <div class="form-group form-group-icon-left">
                            <input class="form-control form-control-2" id="phoneNo" placeholder="Phone No" type="text">
                            <span class="spanError form-control-2" id="spanContactPhone"></span>
                        </div>
                        <div class="form-group form-group-icon-left">
                            <input class="form-control form-control-2" id="institute" placeholder="College / University" type="text">
                            <span class="spanError form-control-2" id="spanContactLocation"></span>
                        </div>
                        <div class="form-group form-group-icon-left">
                            <select id="vehicle_type" class="form-control form-control-2">
                                <option disabled selected value="no">Vehicle Type</option>
                                <option value="Rickshaw">Rickshaw</option>
                                <option value="Other">Others</option>
                            </select>
                            <span class="spanError form-control-2" id="spanVehicleType"></span>
                        </div>
                        <div class="form-group form-group-icon-left">
                            <textarea id="description" class="form-control form-control-2" placeholder="Description"></textarea>
                            <span class="spanError form-control-2"></span>
                        </div>
                        <div class="form-group form-group-icon-left">
                            <input class="btn btn-primary mt10-1" id="contact_pick" value="Send" type="submit">

                            <div id="divMessage" class="alert alert-block fade in"></div>
                        </div>
                    </form>
                  </aside>
              </div>
          </div>
  		</div>
  	</div>
  </div>
<script type="text/javascript">
  $("#contact_pick").click(function(event) {
    event.preventDefault();
    name = $("#name").val();
    phoneNo = $("#phoneNo").val();
    institute = $("#institute").val();
    name = $("#name").val();

  });
</script>
<script type="text/javascript">
  $("#contact_pick").click(function(event) {
    event.preventDefault();
    name = $("#name").val();
    phoneNo = $("#phoneNo").val();
    institute = $("#institute").val();
    vehicle_type = $("#vehicle_type").val();
    description = $("#description").val();


    if (name.length < 2) {
      notify('Information','Please enter a valid name');
      return 0;
    }
    if (phoneNo.length < 2) {
      notify('Information','Please enter a valid phone number');
      return 0;
    }
    if (institute.length < 2) {
      notify('Information','Please enter a institute name');
      return 0;
    }
    if (vehicle_type === 'no' || vehicle_type == null) {
      notify('Information','Please Choice vehicle type');
      return 0;
    }
    if (description.length < 3) {
      notify('Information','Please enter a description');
      return 0;
    }

    $.ajax({
      url: '{{route('pickAndDrop.store')}}',
      type: 'POST',
      data: {
        name: name,
        phoneNo: phoneNo,
        institute: institute,
        vehicle_type: vehicle_type,
        description: description,
        _token: "{{ csrf_token() }}"
      }
    })
    .done(function(Response) {
      result = $.parseJSON(Response)

      if (result.status == 0) {
        notify('error',result.message);
      }
      if (result.status == 1) {
        notify('succuess',result.message);
      }
    })
    .fail(function() {
      notify('error','there was issue when completing your request please try again');
    });

  });
</script>

@endsection
