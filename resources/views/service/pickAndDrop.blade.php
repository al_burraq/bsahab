@extends('layouts.app')
@section('title')
  Bsahab - Pick and Drop
@endsection
@section('body')

  <div class="full-r"><img src="img/trancport-facility-header.jpg"  alt="Rental Car Services - Pick and Drop Services - Bhai Sahab transport" title="Rental Car Services - Pick and Drop Services - Bhai Sahab transport"/></div>
  <div class="container" itemscope itemtype="http://schema.org/ProfessionalService">
  	<div class="row" itemprop="description">
  		<div class="col-md-12">
              <div class="col-sm-8 col-md-8 col-lg-9" style="text-align: justify;line-height:27px;padding: 0px 20px 15px;font-size:17px;font-family:'Roboto', arial, helvetica, sans-serif;">
                  <h1 style="margin-top:20px;">Pick and Drop Service in Lahore</h1>

          <p>At times, when you are travelling by yourself and travelling convenience becomes a bit tricky due to so many reasons such as not having a car, which is the most common and reasonable one along with not knowing anyone in the locality that can help you and drop you off at the place you want to be. However, such reasons shouldn’t deter you from trying alternates, maybe you don’t want to disturb your family or need to be somewhere immediate without relying on someone’s help: that’s where Bhai Sahab’s pick and drop service comes in.</p><br />
          <p>While we offer various other travel services such as rentals and tours, we also have branched out to pick and drop services as well, while our pick and drop service is mainly available for Lahore and surrounding areas, that doesn’t mean we compromise on our services, we actually focus on serving our customers to the best of our ability all the while showing you how we strive to be the number one transport service in our motherland.</p>
          <p><b>How Pick and Drop works.</b></p>
          <p>Our service is very simple and easy to comprehend and use, what you do is contact us to arrange a pick and drop service or you can go to the service section on our site, visit our Pick and drop page and leave your name, contact number, the college you are current enrolled in and the type of vehicle you would like to be picked up on along with a description section for any other special needs alongside your request,  our service strives to achieve the best quality it potentially can alongside being very competitive in prices to facilitate students who can’t earn yet or to keep it as cheap as possible as we all know transport can be  quite expensive.</p>
          <p>What differentiates Bhai Sahabs versatile services from the rest is that we provide all types of services under one proof, our pick and drop services in only one of the many equipment in our arsenal, our dedicated teams, our high quality vehicles and our prompt services. Rather than going of the more expensive options for the same comfort, Bhai Sahab will get you to point B from point A in the most convenient and cheaper manner.</p>
              </div>

              <div class="col-sm-4 col-md-4 col-lg-3" style="margin-bottom:100px">
              <div class="row">
                  <h5 class="" style="margin-top: 20px; border-bottom: 2px solid rgb(237, 131, 35); padding-bottom: 5px;">Contact Us for pick and drop service with in Lahore.</h5>
                  <aside class="sidebar-left">
                      <form id="service-contact">
                          <div class="form-group form-group-icon-left">
                              <input maxlength="150" class="form-control form-control-2" value="" id="name" placeholder="Name" type="text">
                              <span class="spanError form-control-2" id="spanContactName"></span>
                          </div>
                          <div class="form-group form-group-icon-left">
                              <input class="form-control form-control-2" id="phoneNo" placeholder="Phone No" type="text">
                              <span class="spanError form-control-2" id="spanContactPhone"></span>
                          </div>
                          <div class="form-group form-group-icon-left">
                              <input class="form-control form-control-2" id="institute" placeholder="College / University" type="text">
                              <span class="spanError form-control-2" id="spanContactLocation"></span>
                          </div>
                          <div class="form-group form-group-icon-left">
                              <select id="vehicle_type" class="form-control form-control-2">
                                  <option disabled selected value="no">Vehicle Type</option>
                                  <option value="Rickshaw">Rickshaw</option>
                                  <option value="Other">Others</option>
                              </select>
                              <span class="spanError form-control-2" id="spanVehicleType"></span>
                          </div>
                          <div class="form-group form-group-icon-left">
                              <textarea id="description" class="form-control form-control-2" placeholder="Description"></textarea>
                              <span class="spanError form-control-2"></span>
                          </div>
                          <div class="form-group form-group-icon-left">
                              <input class="btn btn-primary mt10-1" id="contact_pick" value="Send" type="submit">

                              <div id="divMessage" class="alert alert-block fade in"></div>
                          </div>
                      </form>
                  </aside>
              </div>
          </div>
  		</div>
  	</div>
  </div>
<script type="text/javascript">
  $("#contact_pick").click(function(event) {
    event.preventDefault();
    name = $("#name").val();
    phoneNo = $("#phoneNo").val();
    institute = $("#institute").val();
    vehicle_type = $("#vehicle_type").val();
    description = $("#description").val();


    if (name.length < 2) {
      notify('Information','Please enter a valid name');
      return 0;
    }
    if (phoneNo.length < 2) {
      notify('Information','Please enter a valid phone number');
      return 0;
    }
    if (institute.length < 2) {
      notify('Information','Please enter a institute name');
      return 0;
    }
    if (vehicle_type === 'no' || vehicle_type == null) {
      notify('Information','Please Choice vehicle type');
      return 0;
    }
    if (description.length < 3) {
      notify('Information','Please enter a description');
      return 0;
    }

    $.ajax({
      url: '{{route('pickAndDrop.store')}}',
      type: 'POST',
      data: {
        name: name,
        phoneNo: phoneNo,
        institute: institute,
        vehicle_type: vehicle_type,
        description: description,
        _token: "{{ csrf_token() }}"
      }
    })
    .done(function(Response) {
      result = $.parseJSON(Response)

      if (result.status == 0) {
        notify('error',result.message);
      }
      if (result.status == 1) {
        notify('succuess',result.message);
      }
    })
    .fail(function() {
      notify('error','there was issue when completing your request please try again');
    });

  });
</script>
@endsection
