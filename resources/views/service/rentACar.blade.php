@extends('layouts.app')
@section('title')
  Bsahab - Rent a Car
@endsection
@section('body')

  <div class="full-r"><img src="img/service-banner.jpg"  alt="Rental Car Services - Pick and Drop Services - Bhai Sahab transport" title="Rental Car Services - Pick and Drop Services - Bhai Sahab transport"/></div>
  <div class="container" itemscope itemtype="http://schema.org/ProfessionalService">
  	<div class="row" itemprop="description">
  		<div class="col-md-12">
              <div class="col-sm-8 col-md-8 col-lg-9" style="text-align: justify;line-height:27px;padding: 0px 20px 15px;font-size:17px;font-family:'Roboto', arial, helvetica, sans-serif;">
                  <h1 style="margin-top:20px;">Rent a Car in Lahore Johar Town, Defence and DHA</h1>
                  <p>Bhai sahib transport is a rent-a-car service that aims to fulfill your travel needs. We offer diverse packages of all types of cars for your travel convenience such as cars for your family outings or if you require more spacious seating arrangement, then we have you covered with our line-up of vans and buses for those long trips with those important ones. We will provide you with all the necessary vehicles for your need and more, by providing accurate in-depth information as to what the rental car services entails.
Amongst the other Rent a car in Lahore, we rank fairly high. If you look at our website, we have vehicle capacity ranging from 5 passengers to 42 passengers, ready for any scenario that you want to undertake for as many people as we can accommodate. For each car, we have included a demo picture along with the necessary info such as:
</p>

<ul>
  <li>The amount of passengers it can accommodate </li>
  <li>The number of doors on the vehicle </li>
  <li>Baggage capacity </li>
  <li>Weather the car is automatic or manual (for the renter’s preference) </li>
  <li>The type of liquid the car consumes (Petrol/Diesel etc.) </li>
  <li>Air conditioned or not. </li>
  <li>Other miscellaneous such as climate control, power locks, multimedia screen etc.</li>

</ul>

                  <p>Alongside stating the features of the potential car that you may want to rent, we have also included prices, these prices shows the rental fee for the car for the day. This would easily portray the price for to be compared to your budget so that you can get the most of the deal, by striking a balance between cost and comfort. While our prices are low which would go to show that we run a cheap Rent a car in Lahore but the less prices are due to being competitive. </p>

                  <p>Lastly, why should you choose us. Bhai Sahab is one of the best rental companies around town, with such a deep inventory when it comes to offering a wide selection of events, a well-detailed website for your convenience where every single detail is explained aesthetically.
                  <p>Our Motto ‘Explore Pakistan with comfort’ is something that we take pride in as our products are well-maintained so that the same rental car would be in its best shape when serving a later customer as we believe in that comfort and quality go hand in hand in the rental business, how we treat our vehicles goes to show how much we care.


              </div>


  		</div>
  	</div>
  </div>
<script type="text/javascript">
  $("#contact_pick").click(function(event) {
    event.preventDefault();
    name = $("#name").val();
    phoneNo = $("#phoneNo").val();
    institute = $("#institute").val();
    name = $("#name").val();

  });
</script>
@endsection
