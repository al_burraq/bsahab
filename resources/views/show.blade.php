@extends('layouts.app')
@section('title','Rent a Car | Economy Car Rental Deals | Car for Rent')
@section('body')
  <link href="//cdn.quilljs.com/1.3.6/quill.core.css" rel="stylesheet">

<style type="text/css">
     #map {
        height: 350px;
		margin-bottom:50px;
      }
	  .iw-content img
	  {
		  width:70px !important;
	  }

#map img {
	max-width: none !important;
}
.gm-style-iw {


	left:21px !important;

}

#iw-container .iw-title {
	font-family: 'Open Sans Condensed', sans-serif;
	font-size: 22px;
	font-weight: 400;
	padding: 10px;
	background-color: #48b5e9;
	color: white;
	margin: 0;
	border-radius: 2px 2px 0 0;
}

.iw-content img {
	float: right;
	margin: 0 5px 5px 10px;
}
.iw-subTitle {
	font-size: 16px;
	font-weight: 700;
	padding: 5px 0;
}
.iw-bottom-gradient {
	position: absolute;
	width: 326px;
	height: 25px;
	bottom: 10px;
	right: 18px;
	background: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -moz-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
	background: -ms-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
}

.detinationImage {
  z-index: 1;
}
.overlay {
    position: absolute;
    top: 0;
    height: 173px;
    width: 90%;
  	z-index: 3;
    opacity: 0.5;
}
</style>
<style media="screen">
.fotorama__stage__frame.fotorama__active {
  z-index: 8;
}
.fotorama__video-play{
  /*position:relative;*/
  z-index:100000;
  /*left:-50px;
  top:10px;*/
  background:url('/img/youtube.png');
  background-size: contain;
  /*width: 20px;
  height: 20px;
  display:block;*/
}
</style>
<div id="map"></div>

<div class="container">

</div>

<script type="text/javascript">
$(document).ready(function() {
var map;
  var latlng = new google.maps.LatLng(30.0706, 71.0937);
  var myOptions = {
  zoom: 5,
  center: latlng,
  draggable: false,
  mapTypeId: google.maps.MapTypeId.ROADMAP,
};
map = new google.maps.Map(document.getElementById("map"), myOptions);

var marker = new google.maps.Marker
(
    {
        position: new google.maps.LatLng({{$destination->latitude}},{{$destination->longitude}}),
        map: map,
        title: '{{$destination->title}}',
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    }
);


});

</script>
<div class="container">





<div class="row">
<div class="col-md-6" style="    margin-bottom: 59px;">
	<h1>{{$destination->title}}</h1>
  <a href="mailto:bhaisahabtrans@gmail.com"><i class="fa fa-envelope"></i> E-mail Car Agent</a>
  <span style="padding-left: 10px;"><i class="fa fa-phone"></i> +92-320-440-0993‬</span>
<br>
<div class="fotorama" data-width="700" data-ratio="700/467" data-max-width="100%" data-allowfullscreen="true"  data-loop="true"   data-nav="thumbs">
    @if($destination->videoThumbnail != null)
      <a href='{{ preg_replace("/^http:/i", "https:", $destination->videoLink)}}'   data-video="true">Rose</a>

    @endif
    <a href='{{asset("img/$destination->image")}}'><img src='{{asset("img/$destination->image")}}' width='494'     height='370'></a>
    @foreach ($destinationGallery as $obj2)
      <a href='{{asset("img/$destination->destination_id/$obj2->image")}}'><img src='{{asset("img/$destination->destination_id/$obj2->image")}}' width='494'     height='370'></a>
    @endforeach
</div>


</div>

<div class="col-md-6" style="margin-top:150px;">
  <div class="pull-right">
		    <p>
          @if (Request::ip() == "115.186.32.40")



          {!! $destination->description !!}

          @else
            {!! $destination->description !!}
          @endif
        	</p>
  </div>
</div>

</div>

	<div class="row row-wrap">
	</div>

</div>


</div>


@endsection
